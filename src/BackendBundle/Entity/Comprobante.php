<?php

namespace BackendBundle\Entity;

/**
 * Comprobante
 */
class Comprobante
{
    /**
     * @var integer
     */
    private $idCom;

    /**
     * @var string
     */
    private $nfcCom;

    /**
     * @var integer
     */
    private $desdeCom;

    /**
     * @var integer
     */
    private $hastaCom;

    /**
     * @var integer
     */
    private $ultimoCom = '0';

    /**
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Get idCom
     *
     * @return integer
     */
    public function getIdCom()
    {
        return $this->idCom;
    }

    /**
     * Set nfcCom
     *
     * @param string $nfcCom
     *
     * @return Comprobante
     */
    public function setNfcCom($nfcCom)
    {
        $this->nfcCom = $nfcCom;

        return $this;
    }

    /**
     * Get nfcCom
     *
     * @return string
     */
    public function getNfcCom()
    {
        return $this->nfcCom;
    }

    /**
     * Set desdeCom
     *
     * @param integer $desdeCom
     *
     * @return Comprobante
     */
    public function setDesdeCom($desdeCom)
    {
        $this->desdeCom = $desdeCom;

        return $this;
    }

    /**
     * Get desdeCom
     *
     * @return integer
     */
    public function getDesdeCom()
    {
        return $this->desdeCom;
    }

    /**
     * Set hastaCom
     *
     * @param integer $hastaCom
     *
     * @return Comprobante
     */
    public function setHastaCom($hastaCom)
    {
        $this->hastaCom = $hastaCom;

        return $this;
    }

    /**
     * Get hastaCom
     *
     * @return integer
     */
    public function getHastaCom()
    {
        return $this->hastaCom;
    }

    /**
     * Set ultimoCom
     *
     * @param integer $ultimoCom
     *
     * @return Comprobante
     */
    public function setUltimoCom($ultimoCom)
    {
        $this->ultimoCom = $ultimoCom;

        return $this;
    }

    /**
     * Get ultimoCom
     *
     * @return integer
     */
    public function getUltimoCom()
    {
        return $this->ultimoCom;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Comprobante
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
