<?php

namespace BackendBundle\Entity;

/**
 * Configuracion
 */
class Configuracion
{
    /**
     * @var integer
     */
    private $idConf;

    /**
     * @var string
     */
    private $nombreConf;

    /**
     * @var string
     */
    private $telefonoConf;

    /**
     * @var string
     */
    private $direccionConf;

    /**
     * @var string
     */
    private $rncConf;

    /**
     * @var string
     */
    private $pieConf;


    /**
     * Get idConf
     *
     * @return integer
     */
    public function getIdConf()
    {
        return $this->idConf;
    }

    /**
     * Set nombreConf
     *
     * @param string $nombreConf
     *
     * @return Configuracion
     */
    public function setNombreConf($nombreConf)
    {
        $this->nombreConf = $nombreConf;

        return $this;
    }

    /**
     * Get nombreConf
     *
     * @return string
     */
    public function getNombreConf()
    {
        return $this->nombreConf;
    }

    /**
     * Set telefonoConf
     *
     * @param string $telefonoConf
     *
     * @return Configuracion
     */
    public function setTelefonoConf($telefonoConf)
    {
        $this->telefonoConf = $telefonoConf;

        return $this;
    }

    /**
     * Get telefonoConf
     *
     * @return string
     */
    public function getTelefonoConf()
    {
        return $this->telefonoConf;
    }

    /**
     * Set direccionConf
     *
     * @param string $direccionConf
     *
     * @return Configuracion
     */
    public function setDireccionConf($direccionConf)
    {
        $this->direccionConf = $direccionConf;

        return $this;
    }

    /**
     * Get direccionConf
     *
     * @return string
     */
    public function getDireccionConf()
    {
        return $this->direccionConf;
    }

    /**
     * Set rncConf
     *
     * @param string $rncConf
     *
     * @return Configuracion
     */
    public function setRncConf($rncConf)
    {
        $this->rncConf = $rncConf;

        return $this;
    }

    /**
     * Get rncConf
     *
     * @return string
     */
    public function getRncConf()
    {
        return $this->rncConf;
    }

    /**
     * Set pieConf
     *
     * @param string $pieConf
     *
     * @return Configuracion
     */
    public function setPieConf($pieConf)
    {
        $this->pieConf = $pieConf;

        return $this;
    }

    /**
     * Get pieConf
     *
     * @return string
     */
    public function getPieConf()
    {
        return $this->pieConf;
    }
}

