<?php

namespace BackendBundle\Entity;

/**
 * DetalleFactura
 */
class DetalleFactura
{
    /**
     * @var integer
     */
    private $idDet;

    /**
     * @var integer
     */
    private $cantidadDet;

    /**
     * @var string
     */
    private $montoDet;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \BackendBundle\Entity\Factura
     */
    private $idFac;

    /**
     * @var \BackendBundle\Entity\Producto
     */
    private $idProd;


    /**
     * Get idDet
     *
     * @return integer
     */
    public function getIdDet()
    {
        return $this->idDet;
    }

    /**
     * Set cantidadDet
     *
     * @param integer $cantidadDet
     *
     * @return DetalleFactura
     */
    public function setCantidadDet($cantidadDet)
    {
        $this->cantidadDet = $cantidadDet;

        return $this;
    }

    /**
     * Get cantidadDet
     *
     * @return integer
     */
    public function getCantidadDet()
    {
        return $this->cantidadDet;
    }

    /**
     * Set montoDet
     *
     * @param string $montoDet
     *
     * @return DetalleFactura
     */
    public function setMontoDet($montoDet)
    {
        $this->montoDet = $montoDet;

        return $this;
    }

    /**
     * Get montoDet
     *
     * @return string
     */
    public function getMontoDet()
    {
        return $this->montoDet;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return DetalleFactura
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set idFac
     *
     * @param \BackendBundle\Entity\Factura $idFac
     *
     * @return DetalleFactura
     */
    public function setIdFac(\BackendBundle\Entity\Factura $idFac = null)
    {
        $this->idFac = $idFac;

        return $this;
    }

    /**
     * Get idFac
     *
     * @return \BackendBundle\Entity\Factura
     */
    public function getIdFac()
    {
        return $this->idFac;
    }

    /**
     * Set idProd
     *
     * @param \BackendBundle\Entity\Producto $idProd
     *
     * @return DetalleFactura
     */
    public function setIdProd(\BackendBundle\Entity\Producto $idProd = null)
    {
        $this->idProd = $idProd;

        return $this;
    }

    /**
     * Get idProd
     *
     * @return \BackendBundle\Entity\Producto
     */
    public function getIdProd()
    {
        return $this->idProd;
    }
}
