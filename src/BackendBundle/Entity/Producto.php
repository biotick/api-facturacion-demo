<?php

namespace BackendBundle\Entity;

/**
 * Producto
 */
class Producto
{
    /**
     * @var integer
     */
    private $idProd;

    /**
     * @var string
     */
    private $nombreProd;

    /**
     * @var integer
     */
    private $salida1Prod;

    /**
     * @var integer
     */
    private $salida2Prod;

    /**
     * @var integer
     */
    private $salida3Prod;

    /**
     * @var integer
     */
    private $estadoProd;

    /**
     * @var integer
     */
    private $codigoProd;

    /**
     * @var integer
     */
    private $itbisProd = '0';

    /**
     * @var string
     */
    private $createdAt;

    /**
     * @var \BackendBundle\Entity\Categoria
     */
    private $idCat;


    /**
     * Get idProd
     *
     * @return integer
     */
    public function getIdProd()
    {
        return $this->idProd;
    }

    /**
     * Set nombreProd
     *
     * @param string $nombreProd
     *
     * @return Producto
     */
    public function setNombreProd($nombreProd)
    {
        $this->nombreProd = $nombreProd;

        return $this;
    }

    /**
     * Get nombreProd
     *
     * @return string
     */
    public function getNombreProd()
    {
        return $this->nombreProd;
    }

    /**
     * Set salida1Prod
     *
     * @param integer $salida1Prod
     *
     * @return Producto
     */
    public function setSalida1Prod($salida1Prod)
    {
        $this->salida1Prod = $salida1Prod;

        return $this;
    }

    /**
     * Get salida1Prod
     *
     * @return integer
     */
    public function getSalida1Prod()
    {
        return $this->salida1Prod;
    }

    /**
     * Set salida2Prod
     *
     * @param integer $salida2Prod
     *
     * @return Producto
     */
    public function setSalida2Prod($salida2Prod)
    {
        $this->salida2Prod = $salida2Prod;

        return $this;
    }

    /**
     * Get salida2Prod
     *
     * @return integer
     */
    public function getSalida2Prod()
    {
        return $this->salida2Prod;
    }

    /**
     * Set salida3Prod
     *
     * @param integer $salida3Prod
     *
     * @return Producto
     */
    public function setSalida3Prod($salida3Prod)
    {
        $this->salida3Prod = $salida3Prod;

        return $this;
    }

    /**
     * Get salida3Prod
     *
     * @return integer
     */
    public function getSalida3Prod()
    {
        return $this->salida3Prod;
    }

    /**
     * Set estadoProd
     *
     * @param integer $estadoProd
     *
     * @return Producto
     */
    public function setEstadoProd($estadoProd)
    {
        $this->estadoProd = $estadoProd;

        return $this;
    }

    /**
     * Get estadoProd
     *
     * @return integer
     */
    public function getEstadoProd()
    {
        return $this->estadoProd;
    }

    /**
     * Set codigoProd
     *
     * @param integer $codigoProd
     *
     * @return Producto
     */
    public function setCodigoProd($codigoProd)
    {
        $this->codigoProd = $codigoProd;

        return $this;
    }

    /**
     * Get codigoProd
     *
     * @return integer
     */
    public function getCodigoProd()
    {
        return $this->codigoProd;
    }

    /**
     * Set itbisProd
     *
     * @param integer $itbisProd
     *
     * @return Producto
     */
    public function setItbisProd($itbisProd)
    {
        $this->itbisProd = $itbisProd;

        return $this;
    }

    /**
     * Get itbisProd
     *
     * @return integer
     */
    public function getItbisProd()
    {
        return $this->itbisProd;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Producto
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set idCat
     *
     * @param \BackendBundle\Entity\Categoria $idCat
     *
     * @return Producto
     */
    public function setIdCat(\BackendBundle\Entity\Categoria $idCat = null)
    {
        $this->idCat = $idCat;

        return $this;
    }

    /**
     * Get idCat
     *
     * @return \BackendBundle\Entity\Categoria
     */
    public function getIdCat()
    {
        return $this->idCat;
    }
    /**
     * @var string
     */
    private $codigopaProd;

    /**
     * @var integer
     */
    private $cantidadpaProd = '0';

    /**
     * @var integer
     */
    private $preciopaProd = '0';


    /**
     * Set codigopaProd
     *
     * @param string $codigopaProd
     *
     * @return Producto
     */
    public function setCodigopaProd($codigopaProd)
    {
        $this->codigopaProd = $codigopaProd;

        return $this;
    }

    /**
     * Get codigopaProd
     *
     * @return string
     */
    public function getCodigopaProd()
    {
        return $this->codigopaProd;
    }

    /**
     * Set cantidadpaProd
     *
     * @param integer $cantidadpaProd
     *
     * @return Producto
     */
    public function setCantidadpaProd($cantidadpaProd)
    {
        $this->cantidadpaProd = $cantidadpaProd;

        return $this;
    }

    /**
     * Get cantidadpaProd
     *
     * @return integer
     */
    public function getCantidadpaProd()
    {
        return $this->cantidadpaProd;
    }

    /**
     * Set preciopaProd
     *
     * @param integer $preciopaProd
     *
     * @return Producto
     */
    public function setPreciopaProd($preciopaProd)
    {
        $this->preciopaProd = $preciopaProd;

        return $this;
    }

    /**
     * Get preciopaProd
     *
     * @return integer
     */
    public function getPreciopaProd()
    {
        return $this->preciopaProd;
    }
    /**
     * @var integer
     */
    private $cantidadcaProd;

    /**
     * @var string
     */
    private $preciocaProd;


    /**
     * Set cantidadcaProd
     *
     * @param integer $cantidadcaProd
     *
     * @return Producto
     */
    public function setCantidadcaProd($cantidadcaProd)
    {
        $this->cantidadcaProd = $cantidadcaProd;

        return $this;
    }

    /**
     * Get cantidadcaProd
     *
     * @return integer
     */
    public function getCantidadcaProd()
    {
        return $this->cantidadcaProd;
    }

    /**
     * Set preciocaProd
     *
     * @param string $preciocaProd
     *
     * @return Producto
     */
    public function setPreciocaProd($preciocaProd)
    {
        $this->preciocaProd = $preciocaProd;

        return $this;
    }

    /**
     * Get preciocaProd
     *
     * @return string
     */
    public function getPreciocaProd()
    {
        return $this->preciocaProd;
    }
    /**
     * @var string
     */
    private $especialProd;


    /**
     * Set especialProd
     *
     * @param string $especialProd
     *
     * @return Producto
     */
    public function setEspecialProd($especialProd)
    {
        $this->especialProd = $especialProd;

        return $this;
    }

    /**
     * Get especialProd
     *
     * @return string
     */
    public function getEspecialProd()
    {
        return $this->especialProd;
    }
}
