<?php

namespace BackendBundle\Entity;

/**
 * Categoria
 */
class Categoria
{
    /**
     * @var integer
     */
    private $idCat;

    /**
     * @var string
     */
    private $nombreCat;

    /**
     * @var integer
     */
    private $tipoCat;

    /**
     * @var integer
     */
    private $estadoCat;

    /**
     * @var \DateTime
     */
    private $createdAt;


    /**
     * Get idCat
     *
     * @return integer
     */
    public function getIdCat()
    {
        return $this->idCat;
    }

    /**
     * Set nombreCat
     *
     * @param string $nombreCat
     *
     * @return Categoria
     */
    public function setNombreCat($nombreCat)
    {
        $this->nombreCat = $nombreCat;

        return $this;
    }

    /**
     * Get nombreCat
     *
     * @return string
     */
    public function getNombreCat()
    {
        return $this->nombreCat;
    }

    /**
     * Set tipoCat
     *
     * @param integer $tipoCat
     *
     * @return Categoria
     */
    public function setTipoCat($tipoCat)
    {
        $this->tipoCat = $tipoCat;

        return $this;
    }

    /**
     * Get tipoCat
     *
     * @return integer
     */
    public function getTipoCat()
    {
        return $this->tipoCat;
    }

    /**
     * Set estadoCat
     *
     * @param integer $estadoCat
     *
     * @return Categoria
     */
    public function setEstadoCat($estadoCat)
    {
        $this->estadoCat = $estadoCat;

        return $this;
    }

    /**
     * Get estadoCat
     *
     * @return integer
     */
    public function getEstadoCat()
    {
        return $this->estadoCat;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Categoria
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
