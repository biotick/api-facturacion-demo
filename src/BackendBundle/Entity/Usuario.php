<?php

namespace BackendBundle\Entity;

/**
 * Usuario
 */
class Usuario
{
    /**
     * @var integer
     */
    private $idUsu;

    /**
     * @var string
     */
    private $nombreUsu;

    /**
     * @var string
     */
    private $apellidoUsu;

    /**
     * @var integer
     */
    private $estadoUsu;

    /**
     * @var integer
     */
    private $tipoUsu;

    /**
     * @var string
     */
    private $nickUsu;

    /**
     * @var string
     */
    private $claveUsu;

    /**
     * @var \DateTime
     */
    private $createdAt;


    /**
     * Get idUsu
     *
     * @return integer
     */
    public function getIdUsu()
    {
        return $this->idUsu;
    }

    /**
     * Set nombreUsu
     *
     * @param string $nombreUsu
     *
     * @return Usuario
     */
    public function setNombreUsu($nombreUsu)
    {
        $this->nombreUsu = $nombreUsu;

        return $this;
    }

    /**
     * Get nombreUsu
     *
     * @return string
     */
    public function getNombreUsu()
    {
        return $this->nombreUsu;
    }

    /**
     * Set apellidoUsu
     *
     * @param string $apellidoUsu
     *
     * @return Usuario
     */
    public function setApellidoUsu($apellidoUsu)
    {
        $this->apellidoUsu = $apellidoUsu;

        return $this;
    }

    /**
     * Get apellidoUsu
     *
     * @return string
     */
    public function getApellidoUsu()
    {
        return $this->apellidoUsu;
    }

    /**
     * Set estadoUsu
     *
     * @param integer $estadoUsu
     *
     * @return Usuario
     */
    public function setEstadoUsu($estadoUsu)
    {
        $this->estadoUsu = $estadoUsu;

        return $this;
    }

    /**
     * Get estadoUsu
     *
     * @return integer
     */
    public function getEstadoUsu()
    {
        return $this->estadoUsu;
    }

    /**
     * Set tipoUsu
     *
     * @param integer $tipoUsu
     *
     * @return Usuario
     */
    public function setTipoUsu($tipoUsu)
    {
        $this->tipoUsu = $tipoUsu;

        return $this;
    }

    /**
     * Get tipoUsu
     *
     * @return integer
     */
    public function getTipoUsu()
    {
        return $this->tipoUsu;
    }

    /**
     * Set nickUsu
     *
     * @param string $nickUsu
     *
     * @return Usuario
     */
    public function setNickUsu($nickUsu)
    {
        $this->nickUsu = $nickUsu;

        return $this;
    }

    /**
     * Get nickUsu
     *
     * @return string
     */
    public function getNickUsu()
    {
        return $this->nickUsu;
    }

    /**
     * Set claveUsu
     *
     * @param string $claveUsu
     *
     * @return Usuario
     */
    public function setClaveUsu($claveUsu)
    {
        $this->claveUsu = $claveUsu;

        return $this;
    }

    /**
     * Get claveUsu
     *
     * @return string
     */
    public function getClaveUsu()
    {
        return $this->claveUsu;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Usuario
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
