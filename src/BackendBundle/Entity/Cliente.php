<?php

namespace BackendBundle\Entity;

/**
 * Cliente
 */
class Cliente
{
    /**
     * @var integer
     */
    private $idCli;

    /**
     * @var string
     */
    private $nombreCli;

    /**
     * @var string
     */
    private $apellidoCli;

    /**
     * @var string
     */
    private $direccionCli;

    /**
     * @var string
     */
    private $telefonoCli;

    /**
     * @var integer
     */
    private $estadoCli;

    /**
     * @var integer
     */
    private $tipoCli;

    /**
     * @var \DateTime
     */
    private $createdAt;


    /**
     * Get idCli
     *
     * @return integer
     */
    public function getIdCli()
    {
        return $this->idCli;
    }

    /**
     * Set nombreCli
     *
     * @param string $nombreCli
     *
     * @return Cliente
     */
    public function setNombreCli($nombreCli)
    {
        $this->nombreCli = $nombreCli;

        return $this;
    }

    /**
     * Get nombreCli
     *
     * @return string
     */
    public function getNombreCli()
    {
        return $this->nombreCli;
    }

    /**
     * Set apellidoCli
     *
     * @param string $apellidoCli
     *
     * @return Cliente
     */
    public function setApellidoCli($apellidoCli)
    {
        $this->apellidoCli = $apellidoCli;

        return $this;
    }

    /**
     * Get apellidoCli
     *
     * @return string
     */
    public function getApellidoCli()
    {
        return $this->apellidoCli;
    }

    /**
     * Set direccionCli
     *
     * @param string $direccionCli
     *
     * @return Cliente
     */
    public function setDireccionCli($direccionCli)
    {
        $this->direccionCli = $direccionCli;

        return $this;
    }

    /**
     * Get direccionCli
     *
     * @return string
     */
    public function getDireccionCli()
    {
        return $this->direccionCli;
    }

    /**
     * Set telefonoCli
     *
     * @param string $telefonoCli
     *
     * @return Cliente
     */
    public function setTelefonoCli($telefonoCli)
    {
        $this->telefonoCli = $telefonoCli;

        return $this;
    }

    /**
     * Get telefonoCli
     *
     * @return string
     */
    public function getTelefonoCli()
    {
        return $this->telefonoCli;
    }

    /**
     * Set estadoCli
     *
     * @param integer $estadoCli
     *
     * @return Cliente
     */
    public function setEstadoCli($estadoCli)
    {
        $this->estadoCli = $estadoCli;

        return $this;
    }

    /**
     * Get estadoCli
     *
     * @return integer
     */
    public function getEstadoCli()
    {
        return $this->estadoCli;
    }

    /**
     * Set tipoCli
     *
     * @param integer $tipoCli
     *
     * @return Cliente
     */
    public function setTipoCli($tipoCli)
    {
        $this->tipoCli = $tipoCli;

        return $this;
    }

    /**
     * Get tipoCli
     *
     * @return integer
     */
    public function getTipoCli()
    {
        return $this->tipoCli;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Cliente
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
