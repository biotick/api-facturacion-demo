<?php

namespace BackendBundle\Entity;

/**
 * Factura
 */
class Factura
{
    /**
     * @var integer
     */
    private $idFac;

    /**
     * @var integer
     */
    private $codigoFac;

    /**
     * @var string
     */
    private $fechaFac;

    /**
     * @var integer
     */
    private $condicionFac;

    /**
     * @var integer
     */
    private $descuentoFac;

    /**
     * @var integer
     */
    private $estadoFac;

    /**
     * @var string
     */
    private $subtotalFac;

    /**
     * @var string
     */
    private $totalFac;

    /**
     * @var string
     */
    private $nfcFac;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \BackendBundle\Entity\Cliente
     */
    private $idCli;

    /**
     * @var \BackendBundle\Entity\Usuario
     */
    private $idUsu;


    /**
     * Get idFac
     *
     * @return integer
     */
    public function getIdFac()
    {
        return $this->idFac;
    }

    /**
     * Set codigoFac
     *
     * @param integer $codigoFac
     *
     * @return Factura
     */
    public function setCodigoFac($codigoFac)
    {
        $this->codigoFac = $codigoFac;

        return $this;
    }

    /**
     * Get codigoFac
     *
     * @return integer
     */
    public function getCodigoFac()
    {
        return $this->codigoFac;
    }

    /**
     * Set fechaFac
     *
     * @param string $fechaFac
     *
     * @return Factura
     */
    public function setFechaFac($fechaFac)
    {
        $this->fechaFac = $fechaFac;

        return $this;
    }

    /**
     * Get fechaFac
     *
     * @return string
     */
    public function getFechaFac()
    {
        return $this->fechaFac;
    }

    /**
     * Set condicionFac
     *
     * @param integer $condicionFac
     *
     * @return Factura
     */
    public function setCondicionFac($condicionFac)
    {
        $this->condicionFac = $condicionFac;

        return $this;
    }

    /**
     * Get condicionFac
     *
     * @return integer
     */
    public function getCondicionFac()
    {
        return $this->condicionFac;
    }

    /**
     * Set descuentoFac
     *
     * @param integer $descuentoFac
     *
     * @return Factura
     */
    public function setDescuentoFac($descuentoFac)
    {
        $this->descuentoFac = $descuentoFac;

        return $this;
    }

    /**
     * Get descuentoFac
     *
     * @return integer
     */
    public function getDescuentoFac()
    {
        return $this->descuentoFac;
    }

    /**
     * Set estadoFac
     *
     * @param integer $estadoFac
     *
     * @return Factura
     */
    public function setEstadoFac($estadoFac)
    {
        $this->estadoFac = $estadoFac;

        return $this;
    }

    /**
     * Get estadoFac
     *
     * @return integer
     */
    public function getEstadoFac()
    {
        return $this->estadoFac;
    }

    /**
     * Set subtotalFac
     *
     * @param string $subtotalFac
     *
     * @return Factura
     */
    public function setSubtotalFac($subtotalFac)
    {
        $this->subtotalFac = $subtotalFac;

        return $this;
    }

    /**
     * Get subtotalFac
     *
     * @return string
     */
    public function getSubtotalFac()
    {
        return $this->subtotalFac;
    }

    /**
     * Set totalFac
     *
     * @param string $totalFac
     *
     * @return Factura
     */
    public function setTotalFac($totalFac)
    {
        $this->totalFac = $totalFac;

        return $this;
    }

    /**
     * Get totalFac
     *
     * @return string
     */
    public function getTotalFac()
    {
        return $this->totalFac;
    }

    /**
     * Set nfcFac
     *
     * @param string $nfcFac
     *
     * @return Factura
     */
    public function setNfcFac($nfcFac)
    {
        $this->nfcFac = $nfcFac;

        return $this;
    }

    /**
     * Get nfcFac
     *
     * @return string
     */
    public function getNfcFac()
    {
        return $this->nfcFac;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Factura
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set idCli
     *
     * @param \BackendBundle\Entity\Cliente $idCli
     *
     * @return Factura
     */
    public function setIdCli(\BackendBundle\Entity\Cliente $idCli = null)
    {
        $this->idCli = $idCli;

        return $this;
    }

    /**
     * Get idCli
     *
     * @return \BackendBundle\Entity\Cliente
     */
    public function getIdCli()
    {
        return $this->idCli;
    }

    /**
     * Set idUsu
     *
     * @param \BackendBundle\Entity\Usuario $idUsu
     *
     * @return Factura
     */
    public function setIdUsu(\BackendBundle\Entity\Usuario $idUsu = null)
    {
        $this->idUsu = $idUsu;

        return $this;
    }

    /**
     * Get idUsu
     *
     * @return \BackendBundle\Entity\Usuario
     */
    public function getIdUsu()
    {
        return $this->idUsu;
    }
    /**
     * @var string
     */
    private $rncFac;

    /**
     * @var integer
     */
    private $tipoFac;


    /**
     * Set rncFac
     *
     * @param string $rncFac
     *
     * @return Factura
     */
    public function setRncFac($rncFac)
    {
        $this->rncFac = $rncFac;

        return $this;
    }

    /**
     * Get rncFac
     *
     * @return string
     */
    public function getRncFac()
    {
        return $this->rncFac;
    }

    /**
     * Set tipoFac
     *
     * @param integer $tipoFac
     *
     * @return Factura
     */
    public function setTipoFac($tipoFac)
    {
        $this->tipoFac = $tipoFac;

        return $this;
    }

    /**
     * Get tipoFac
     *
     * @return integer
     */
    public function getTipoFac()
    {
        return $this->tipoFac;
    }
}
