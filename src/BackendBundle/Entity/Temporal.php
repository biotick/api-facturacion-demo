<?php

namespace BackendBundle\Entity;

/**
 * Temporal
 */
class Temporal
{
    /**
     * @var integer
     */
    private $idTem;

    /**
     * @var integer
     */
    private $cantidadTem;

    /**
     * @var \BackendBundle\Entity\Producto
     */
    private $idPro;


    /**
     * Get idTem
     *
     * @return integer
     */
    public function getIdTem()
    {
        return $this->idTem;
    }

    /**
     * Set cantidadTem
     *
     * @param integer $cantidadTem
     *
     * @return Temporal
     */
    public function setCantidadTem($cantidadTem)
    {
        $this->cantidadTem = $cantidadTem;

        return $this;
    }

    /**
     * Get cantidadTem
     *
     * @return integer
     */
    public function getCantidadTem()
    {
        return $this->cantidadTem;
    }

    /**
     * Set idPro
     *
     * @param \BackendBundle\Entity\Producto $idPro
     *
     * @return Temporal
     */
    public function setIdPro(\BackendBundle\Entity\Producto $idPro = null)
    {
        $this->idPro = $idPro;

        return $this;
    }

    /**
     * Get idPro
     *
     * @return \BackendBundle\Entity\Producto
     */
    public function getIdPro()
    {
        return $this->idPro;
    }
    /**
     * @var integer
     */
    private $precioTem;


    /**
     * Set precioTem
     *
     * @param integer $precioTem
     *
     * @return Temporal
     */
    public function setPrecioTem($precioTem)
    {
        $this->precioTem = $precioTem;

        return $this;
    }

    /**
     * Get precioTem
     *
     * @return integer
     */
    public function getPrecioTem()
    {
        return $this->precioTem;
    }
    /**
     * @var \BackendBundle\Entity\Usuario
     */
    private $idUsu;


    /**
     * Set idUsu
     *
     * @param \BackendBundle\Entity\Usuario $idUsu
     *
     * @return Temporal
     */
    public function setIdUsu(\BackendBundle\Entity\Usuario $idUsu = null)
    {
        $this->idUsu = $idUsu;

        return $this;
    }

    /**
     * Get idUsu
     *
     * @return \BackendBundle\Entity\Usuario
     */
    public function getIdUsu()
    {
        return $this->idUsu;
    }
}
