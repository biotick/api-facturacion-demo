<?php

namespace BackendBundle\Entity;

/**
 * CuentaPp
 */
class CuentaPp
{
    /**
     * @var integer
     */
    private $idCupp;

    /**
     * @var string
     */
    private $conceptoCupp;

    /**
     * @var integer
     */
    private $totalCupp;

    /**
     * @var integer
     */
    private $estadoCupp;

    /**
     * @var \BackendBundle\Entity\Proveedor
     */
    private $idProv;


    /**
     * Get idCupp
     *
     * @return integer
     */
    public function getIdCupp()
    {
        return $this->idCupp;
    }

    /**
     * Set conceptoCupp
     *
     * @param string $conceptoCupp
     *
     * @return CuentaPp
     */
    public function setConceptoCupp($conceptoCupp)
    {
        $this->conceptoCupp = $conceptoCupp;

        return $this;
    }

    /**
     * Get conceptoCupp
     *
     * @return string
     */
    public function getConceptoCupp()
    {
        return $this->conceptoCupp;
    }

    /**
     * Set totalCupp
     *
     * @param integer $totalCupp
     *
     * @return CuentaPp
     */
    public function setTotalCupp($totalCupp)
    {
        $this->totalCupp = $totalCupp;

        return $this;
    }

    /**
     * Get totalCupp
     *
     * @return integer
     */
    public function getTotalCupp()
    {
        return $this->totalCupp;
    }

    /**
     * Set estadoCupp
     *
     * @param integer $estadoCupp
     *
     * @return CuentaPp
     */
    public function setEstadoCupp($estadoCupp)
    {
        $this->estadoCupp = $estadoCupp;

        return $this;
    }

    /**
     * Get estadoCupp
     *
     * @return integer
     */
    public function getEstadoCupp()
    {
        return $this->estadoCupp;
    }

    /**
     * Set idProv
     *
     * @param \BackendBundle\Entity\Proveedor $idProv
     *
     * @return CuentaPp
     */
    public function setIdProv(\BackendBundle\Entity\Proveedor $idProv = null)
    {
        $this->idProv = $idProv;

        return $this;
    }

    /**
     * Get idProv
     *
     * @return \BackendBundle\Entity\Proveedor
     */
    public function getIdProv()
    {
        return $this->idProv;
    }
    /**
     * @var integer
     */
    private $idDetp;


    /**
     * Set idDetp
     *
     * @param integer $idDetp
     *
     * @return CuentaPp
     */
    public function setIdDetp($idDetp)
    {
        $this->idDetp = $idDetp;

        return $this;
    }

    /**
     * Get idDetp
     *
     * @return integer
     */
    public function getIdDetp()
    {
        return $this->idDetp;
    }
}
