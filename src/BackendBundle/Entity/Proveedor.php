<?php

namespace BackendBundle\Entity;

/**
 * Proveedor
 */
class Proveedor
{
    /**
     * @var integer
     */
    private $idProv;

    /**
     * @var string
     */
    private $nombreProv;

    /**
     * @var string
     */
    private $descripcionProv;

    /**
     * @var integer
     */
    private $estadoProv;

    /**
     * @var string
     */
    private $telefono1Prov;

    /**
     * @var string
     */
    private $telefono2Prov;

    /**
     * @var string
     */
    private $telefono3Prov;

    /**
     * @var \DateTime
     */
    private $createdAt;


    /**
     * Get idProv
     *
     * @return integer
     */
    public function getIdProv()
    {
        return $this->idProv;
    }

    /**
     * Set nombreProv
     *
     * @param string $nombreProv
     *
     * @return Proveedor
     */
    public function setNombreProv($nombreProv)
    {
        $this->nombreProv = $nombreProv;

        return $this;
    }

    /**
     * Get nombreProv
     *
     * @return string
     */
    public function getNombreProv()
    {
        return $this->nombreProv;
    }

    /**
     * Set descripcionProv
     *
     * @param string $descripcionProv
     *
     * @return Proveedor
     */
    public function setDescripcionProv($descripcionProv)
    {
        $this->descripcionProv = $descripcionProv;

        return $this;
    }

    /**
     * Get descripcionProv
     *
     * @return string
     */
    public function getDescripcionProv()
    {
        return $this->descripcionProv;
    }

    /**
     * Set estadoProv
     *
     * @param integer $estadoProv
     *
     * @return Proveedor
     */
    public function setEstadoProv($estadoProv)
    {
        $this->estadoProv = $estadoProv;

        return $this;
    }

    /**
     * Get estadoProv
     *
     * @return integer
     */
    public function getEstadoProv()
    {
        return $this->estadoProv;
    }

    /**
     * Set telefono1Prov
     *
     * @param string $telefono1Prov
     *
     * @return Proveedor
     */
    public function setTelefono1Prov($telefono1Prov)
    {
        $this->telefono1Prov = $telefono1Prov;

        return $this;
    }

    /**
     * Get telefono1Prov
     *
     * @return string
     */
    public function getTelefono1Prov()
    {
        return $this->telefono1Prov;
    }

    /**
     * Set telefono2Prov
     *
     * @param string $telefono2Prov
     *
     * @return Proveedor
     */
    public function setTelefono2Prov($telefono2Prov)
    {
        $this->telefono2Prov = $telefono2Prov;

        return $this;
    }

    /**
     * Get telefono2Prov
     *
     * @return string
     */
    public function getTelefono2Prov()
    {
        return $this->telefono2Prov;
    }

    /**
     * Set telefono3Prov
     *
     * @param string $telefono3Prov
     *
     * @return Proveedor
     */
    public function setTelefono3Prov($telefono3Prov)
    {
        $this->telefono3Prov = $telefono3Prov;

        return $this;
    }

    /**
     * Get telefono3Prov
     *
     * @return string
     */
    public function getTelefono3Prov()
    {
        return $this->telefono3Prov;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Proveedor
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
