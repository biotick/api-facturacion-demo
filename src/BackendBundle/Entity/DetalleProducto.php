<?php

namespace BackendBundle\Entity;

/**
 * DetalleProducto
 */
class DetalleProducto
{
    /**
     * @var integer
     */
    private $idDetp;

    /**
     * @var string
     */
    private $fechaDetp;

    /**
     * @var integer
     */
    private $costoDetp;

    /**
     * @var integer
     */
    private $estadoDetp;

    /**
     * @var integer
     */
    private $cantidadDept;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \BackendBundle\Entity\Proveedor
     */
    private $idProv;

    /**
     * @var \BackendBundle\Entity\Producto
     */
    private $idProd;


    /**
     * Get idDetp
     *
     * @return integer
     */
    public function getIdDetp()
    {
        return $this->idDetp;
    }

    /**
     * Set fechaDetp
     *
     * @param string $fechaDetp
     *
     * @return DetalleProducto
     */
    public function setFechaDetp($fechaDetp)
    {
        $this->fechaDetp = $fechaDetp;

        return $this;
    }

    /**
     * Get fechaDetp
     *
     * @return string
     */
    public function getFechaDetp()
    {
        return $this->fechaDetp;
    }

    /**
     * Set costoDetp
     *
     * @param integer $costoDetp
     *
     * @return DetalleProducto
     */
    public function setCostoDetp($costoDetp)
    {
        $this->costoDetp = $costoDetp;

        return $this;
    }

    /**
     * Get costoDetp
     *
     * @return integer
     */
    public function getCostoDetp()
    {
        return $this->costoDetp;
    }

    /**
     * Set estadoDetp
     *
     * @param integer $estadoDetp
     *
     * @return DetalleProducto
     */
    public function setEstadoDetp($estadoDetp)
    {
        $this->estadoDetp = $estadoDetp;

        return $this;
    }

    /**
     * Get estadoDetp
     *
     * @return integer
     */
    public function getEstadoDetp()
    {
        return $this->estadoDetp;
    }

    /**
     * Set cantidadDept
     *
     * @param integer $cantidadDept
     *
     * @return DetalleProducto
     */
    public function setCantidadDept($cantidadDept)
    {
        $this->cantidadDept = $cantidadDept;

        return $this;
    }

    /**
     * Get cantidadDept
     *
     * @return integer
     */
    public function getCantidadDept()
    {
        return $this->cantidadDept;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return DetalleProducto
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set idProv
     *
     * @param \BackendBundle\Entity\Proveedor $idProv
     *
     * @return DetalleProducto
     */
    public function setIdProv(\BackendBundle\Entity\Proveedor $idProv = null)
    {
        $this->idProv = $idProv;

        return $this;
    }

    /**
     * Get idProv
     *
     * @return \BackendBundle\Entity\Proveedor
     */
    public function getIdProv()
    {
        return $this->idProv;
    }

    /**
     * Set idProd
     *
     * @param \BackendBundle\Entity\Producto $idProd
     *
     * @return DetalleProducto
     */
    public function setIdProd(\BackendBundle\Entity\Producto $idProd = null)
    {
        $this->idProd = $idProd;

        return $this;
    }

    /**
     * Get idProd
     *
     * @return \BackendBundle\Entity\Producto
     */
    public function getIdProd()
    {
        return $this->idProd;
    }
    /**
     * @var integer
     */
    private $condicionDept;


    /**
     * Set condicionDept
     *
     * @param integer $condicionDept
     *
     * @return DetalleProducto
     */
    public function setCondicionDept($condicionDept)
    {
        $this->condicionDept = $condicionDept;

        return $this;
    }

    /**
     * Get condicionDept
     *
     * @return integer
     */
    public function getCondicionDept()
    {
        return $this->condicionDept;
    }
    /**
     * @var integer
     */
    private $cantidadDetp;

    /**
     * @var integer
     */
    private $condicionDetp;


    /**
     * Set cantidadDetp
     *
     * @param integer $cantidadDetp
     *
     * @return DetalleProducto
     */
    public function setCantidadDetp($cantidadDetp)
    {
        $this->cantidadDetp = $cantidadDetp;

        return $this;
    }

    /**
     * Get cantidadDetp
     *
     * @return integer
     */
    public function getCantidadDetp()
    {
        return $this->cantidadDetp;
    }

    /**
     * Set condicionDetp
     *
     * @param integer $condicionDetp
     *
     * @return DetalleProducto
     */
    public function setCondicionDetp($condicionDetp)
    {
        $this->condicionDetp = $condicionDetp;

        return $this;
    }

    /**
     * Get condicionDetp
     *
     * @return integer
     */
    public function getCondicionDetp()
    {
        return $this->condicionDetp;
    }
}
