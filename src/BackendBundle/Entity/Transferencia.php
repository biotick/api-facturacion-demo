<?php

namespace BackendBundle\Entity;

/**
 * Transferencia
 */
class Transferencia
{
    /**
     * @var integer
     */
    private $idTrans;

    /**
     * @var string
     */
    private $numeroTrans;

    /**
     * @var integer
     */
    private $valorTrans;

    /**
     * @var integer
     */
    private $estadoTrans;

    /**
     * @var integer
     */
    private $tipoTrans;

    /**
     * @var \BackendBundle\Entity\Cuenta
     */
    private $idCue;


    /**
     * Get idTrans
     *
     * @return integer
     */
    public function getIdTrans()
    {
        return $this->idTrans;
    }

    /**
     * Set numeroTrans
     *
     * @param string $numeroTrans
     *
     * @return Transferencia
     */
    public function setNumeroTrans($numeroTrans)
    {
        $this->numeroTrans = $numeroTrans;

        return $this;
    }

    /**
     * Get numeroTrans
     *
     * @return string
     */
    public function getNumeroTrans()
    {
        return $this->numeroTrans;
    }

    /**
     * Set valorTrans
     *
     * @param integer $valorTrans
     *
     * @return Transferencia
     */
    public function setValorTrans($valorTrans)
    {
        $this->valorTrans = $valorTrans;

        return $this;
    }

    /**
     * Get valorTrans
     *
     * @return integer
     */
    public function getValorTrans()
    {
        return $this->valorTrans;
    }

    /**
     * Set estadoTrans
     *
     * @param integer $estadoTrans
     *
     * @return Transferencia
     */
    public function setEstadoTrans($estadoTrans)
    {
        $this->estadoTrans = $estadoTrans;

        return $this;
    }

    /**
     * Get estadoTrans
     *
     * @return integer
     */
    public function getEstadoTrans()
    {
        return $this->estadoTrans;
    }

    /**
     * Set tipoTrans
     *
     * @param integer $tipoTrans
     *
     * @return Transferencia
     */
    public function setTipoTrans($tipoTrans)
    {
        $this->tipoTrans = $tipoTrans;

        return $this;
    }

    /**
     * Get tipoTrans
     *
     * @return integer
     */
    public function getTipoTrans()
    {
        return $this->tipoTrans;
    }

    /**
     * Set idCue
     *
     * @param \BackendBundle\Entity\Cuenta $idCue
     *
     * @return Transferencia
     */
    public function setIdCue(\BackendBundle\Entity\Cuenta $idCue = null)
    {
        $this->idCue = $idCue;

        return $this;
    }

    /**
     * Get idCue
     *
     * @return \BackendBundle\Entity\Cuenta
     */
    public function getIdCue()
    {
        return $this->idCue;
    }
}
