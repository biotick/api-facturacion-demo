<?php

namespace BackendBundle\Entity;

/**
 * Cuenta
 */
class Cuenta
{
    /**
     * @var integer
     */
    private $idCue;

    /**
     * @var integer
     */
    private $numeroCue;

    /**
     * @var integer
     */
    private $bancoCue;

    /**
     * @var integer
     */
    private $tipoCue;

    /**
     * @var string
     */
    private $nombreCue;

    /**
     * @var integer
     */
    private $estadoCue;

    /**
     * @var \DateTime
     */
    private $createdAt;


    /**
     * Get idCue
     *
     * @return integer
     */
    public function getIdCue()
    {
        return $this->idCue;
    }

    /**
     * Set numeroCue
     *
     * @param integer $numeroCue
     *
     * @return Cuenta
     */
    public function setNumeroCue($numeroCue)
    {
        $this->numeroCue = $numeroCue;

        return $this;
    }

    /**
     * Get numeroCue
     *
     * @return integer
     */
    public function getNumeroCue()
    {
        return $this->numeroCue;
    }

    /**
     * Set bancoCue
     *
     * @param integer $bancoCue
     *
     * @return Cuenta
     */
    public function setBancoCue($bancoCue)
    {
        $this->bancoCue = $bancoCue;

        return $this;
    }

    /**
     * Get bancoCue
     *
     * @return integer
     */
    public function getBancoCue()
    {
        return $this->bancoCue;
    }

    /**
     * Set tipoCue
     *
     * @param integer $tipoCue
     *
     * @return Cuenta
     */
    public function setTipoCue($tipoCue)
    {
        $this->tipoCue = $tipoCue;

        return $this;
    }

    /**
     * Get tipoCue
     *
     * @return integer
     */
    public function getTipoCue()
    {
        return $this->tipoCue;
    }

    /**
     * Set nombreCue
     *
     * @param string $nombreCue
     *
     * @return Cuenta
     */
    public function setNombreCue($nombreCue)
    {
        $this->nombreCue = $nombreCue;

        return $this;
    }

    /**
     * Get nombreCue
     *
     * @return string
     */
    public function getNombreCue()
    {
        return $this->nombreCue;
    }

    /**
     * Set estadoCue
     *
     * @param integer $estadoCue
     *
     * @return Cuenta
     */
    public function setEstadoCue($estadoCue)
    {
        $this->estadoCue = $estadoCue;

        return $this;
    }

    /**
     * Get estadoCue
     *
     * @return integer
     */
    public function getEstadoCue()
    {
        return $this->estadoCue;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Cuenta
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
