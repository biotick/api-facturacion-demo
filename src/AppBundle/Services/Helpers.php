<?php
namespace AppBundle\Services;

class Helpers{
  public $manager;
  public function __construct($manager){
    $this->manager = $manager;
  }


  //metodo para convertir cualquier dato a json
  public function json($data){
    //normalizador de la data para convertirla a json
    $normalizers = array(new \Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer());
    //codificador de la data para convertirla a json
    $encoders = array("json" => new \Symfony\Component\Serializer\Encoder\JsonEncoder());

    $serializer = new \Symfony\Component\Serializer\Serializer($normalizers,$encoders);
    //objeto ya convertido a json
    $json = $serializer->serialize($data,'json');

    //instancia del metodo response para devolver una respuesta valida;
    $response = new \Symfony\Component\HttpFoundation\Response();
    $response->setContent($json);
    $response->headers->set('Content-Type','application/json');

    return $response;
  }
}
