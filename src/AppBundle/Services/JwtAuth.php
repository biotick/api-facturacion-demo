<?php
namespace AppBundle\Services;

use Firebase\JWT\JWT;

class JwtAuth{

  public $manager;
  private $key = "wxedqwzadv123!";

  public function __construct($manager){
    $this->manager = $manager;
  }

  public function singup($nick,$password,$getInfo = null){
    //traigo un objeto del usuario en caso de que las credenciales sean validas
    $user = $this->manager->getRepository('BackendBundle:Usuario')->findOneBy(array(
      'nickUsu' => $nick,'claveUsu' => $password, 'estadoUsu' => 1));

      //si el objeto es valido enciendo el flag
      $singup = false;
      if (is_object($user)) {
        $singup = true;
      }

      //si el flag es true
      if ($singup) {
        //generar token jwt
        $token = array(
          "sub" => $user->getIdUsu(),
          "nombre" => $user->getNombreUsu(),
          "apellido" => $user->getApellidoUsu(),
          'nick' => $user->getNickUsu(),
          'tipo' => $user->getTipoUsu(),
          'estado' => $user->getEstadoUsu(),
          "iat" => time(),
          'exp' => time() + (7 * 24 * 60 * 60 )
        );
        $jwt = JWT::encode($token,$this->key,'HS256');

        if ($getInfo == null) {
          $data = $jwt;
        }else{
          $decoded = JWT::decode($jwt,$this->key,array('HS256'));
          $data = $decoded;
        }

      }else{
        $data = array('status' => 'error',
                        'data' => 'El login ha fallado');
      }
    return $data;
  }

  public function checktoken($jwt,$getIdentity = false){
    $auth = false;
    try {
      $decoded = JWT::decode($jwt,$this->key,array('HS256'));
    } catch (\UnexpectedValueException $e) {
      $auth = false;
    } catch(\DomainException $e){
      $auth = false;
    }

    if(isset($decoded) && is_object($decoded) && isset($decoded->sub)){
      $auth = true;
    }else{
      $auth = false;
    }

    if ($getIdentity == false) {
      return $auth;
    }else{
      return $decoded;
    }


  }
}
