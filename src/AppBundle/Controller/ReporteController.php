<?php
namespace AppBundle\Controller;

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Services\Helpers;
use AppBundle\Services\JwtAuth;
use BackendBundle\Entity\Temporal;

class ReporteController extends Controller {
	public function buscarReporteAction(Request $request){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$json = $request->get('json',null);
			$params = json_decode($json);
			if ($json != null) {
				$estado = (isset($params->estado)) ? $params->estado:null;
				$desde = (isset($params->desde)) ? $params->desde:null;
				$hasta = (isset($params->hasta)) ? $params->hasta:null;
				if ($estado != null && is_numeric($estado) && $desde != null && $hasta != null) {
					$em = $this->getDoctrine()->getManager();
					if ($estado == 1) {
						$dql = "SELECT f.codigoFac,c.nombreCli,c.apellidoCli,f.fechaFac,f.totalFac,f.descuentoFac,u.nombreUsu,f.subtotalFac FROM BackendBundle:Factura f INNER JOIN BackendBundle:Cliente c WITH f.idCli = c.idCli INNER JOIN BackendBundle:Usuario u WITH f.idUsu = u.idUsu WHERE f.estadoFac = 1 AND (f.fechaFac >= :desde AND f.fechaFac <= :hasta)";
					}elseif ($estado == 2) {
						$dql = "SELECT f.codigoFac,c.nombreCli,c.apellidoCli,f.fechaFac,f.totalFac,f.descuentoFac,u.nombreUsu,f.subtotalFac FROM BackendBundle:Factura f INNER JOIN BackendBundle:Cliente c WITH f.idCli = c.idCli INNER JOIN BackendBundle:Usuario u WITH f.idUsu = u.idUsu WHERE f.estadoFac = 2 AND (f.fechaFac >= :desde AND f.fechaFac <= :hasta)";
					}else{
						$dql = "SELECT f.codigoFac,c.nombreCli,c.apellidoCli,f.fechaFac,f.totalFac,f.descuentoFac,u.nombreUsu,f.subtotalFac FROM BackendBundle:Factura f INNER JOIN BackendBundle:Cliente c WITH f.idCli = c.idCli INNER JOIN BackendBundle:Usuario u WITH f.idUsu = u.idUsu WHERE f.estadoFac != 3 AND (f.fechaFac >= :desde AND f.fechaFac <= :hasta)";
					}
					$query = $em->createQuery($dql)->setParameters(array('desde' => $desde,'hasta' => $hasta));
					$facturas = $query->getResult();
					if (count($facturas) > 0) {
						$data = array('status' => 'success',
								  'code' => 200,
								  'data' => $facturas);
					}else{
						$data = array('status' => 'error',
										'code' => 401,
										'data' => "No existen facturas");
					}
					
				}else{
					$data = array("status" => 'error',
									"code" => 400,
									"data" => "Los datos enviados son invalidos");
				}

			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Asegurese de enviar el objeto json');
			} 
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

	public function reportePdfAction(Request $request, $desde,$hasta,$estado,$token){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		if ($jwt_auth->checktoken($token)) {
			if ($desde != null && $hasta != null && $estado != null) {
					$em = $this->getDoctrine()->getManager();
					if ($estado == 1) {
						$dql = "SELECT f.codigoFac,c.nombreCli,c.apellidoCli,f.fechaFac,f.totalFac,f.descuentoFac,u.nombreUsu,f.subtotalFac FROM BackendBundle:Factura f INNER JOIN BackendBundle:Cliente c WITH f.idCli = c.idCli INNER JOIN BackendBundle:Usuario u WITH f.idUsu = u.idUsu WHERE f.estadoFac = 1 AND (f.fechaFac >= :desde AND f.fechaFac <= :hasta)";
					}elseif ($estado == 2) {
						$dql = "SELECT f.codigoFac,c.nombreCli,c.apellidoCli,f.fechaFac,f.totalFac,f.descuentoFac,u.nombreUsu,f.subtotalFac FROM BackendBundle:Factura f INNER JOIN BackendBundle:Cliente c WITH f.idCli = c.idCli INNER JOIN BackendBundle:Usuario u WITH f.idUsu = u.idUsu WHERE f.estadoFac = 2 AND (f.fechaFac >= :desde AND f.fechaFac <= :hasta)";
					}else{
						$dql = "SELECT f.codigoFac,c.nombreCli,c.apellidoCli,f.fechaFac,f.totalFac,f.descuentoFac,u.nombreUsu,f.subtotalFac FROM BackendBundle:Factura f INNER JOIN BackendBundle:Cliente c WITH f.idCli = c.idCli INNER JOIN BackendBundle:Usuario u WITH f.idUsu = u.idUsu WHERE f.estadoFac != 3 AND (f.fechaFac >= :desde AND f.fechaFac <= :hasta)";
					}
					$query = $em->createQuery($dql)->setParameters(array('desde' => $desde,'hasta' => $hasta));
					$facturas = $query->getResult();
					if (count($facturas) > 0) {
							$config = $em->getRepository('BackendBundle:Configuracion')->findOneBy(array('idConf'=>1));
							$tableta = '<tr class="info">';
							$tableta = $tableta.'<th>Codigo</th>';
							$tableta = $tableta.'<th>Cliente</th>';
							$tableta = $tableta.'<th>Fecha</th>';
							$tableta = $tableta.'<th>Total</th>';
							$tableta = $tableta.'</tr>';
							$suma = 0;

							foreach ($facturas as $factura){
							  $suma = $suma + $factura['totalFac'];
							  $tableta = $tableta.'<tr>';
							  $tableta = $tableta.'<td>'.$this->ceros($factura['codigoFac']).'</td>';
							  $tableta = $tableta.'<td>'.$factura['nombreCli'].' '.$factura['apellidoCli'].'</td>';
							  $tableta = $tableta.'<td>'.$factura['fechaFac'].'</td>';
							  $tableta = $tableta.'<td>$'.number_format($factura['totalFac'],2,'.',',').'</td>';
							  $tableta = $tableta.'</tr>';
							}

							$tableta = $tableta.'</table>';
							$tableta = $tableta.'<br>';
							$tableta = $tableta.'<table align="right" class="cont">';
							$tableta = $tableta.'<tr>';
							$tableta = $tableta.'<th class="marg">TOTAL $</th>';
							$tableta = $tableta.'<th class="marg">$'.number_format($suma,2,'.',',').'</th>';
							$tableta = $tableta.'</tr>';
							$tableta = $tableta.'</table>';

							$mpdfService = $this->get('tfox.mpdfport');
							$css = file_get_contents('html/estilosPDF.css');
							$html = file_get_contents('html/pdf.html');
							$html = str_replace('<style></style>','<style>'.$css.'</style>', $html);
							$html = str_replace('{empresa}',$config->getNombreConf(), $html);
							$html = str_replace('{telefono}',$config->getTelefonoConf(), $html);
							$html = str_replace('{direccion}',$config->getDireccionConf(), $html);
							$html = str_replace('{tabla}',$tableta, $html);
							$html = str_replace('{desde}',$desde , $html);
							$html = str_replace('{hasta}',$hasta , $html);
							$texto_estado= "";
							if($estado  == 1){
								  $texto_estado = "Pagado";
							}else if($estado == 2){
								  $texto_estado  = "Pendiente";
							}else{
								$texto_estado = "Todos";
							}
							$html = str_replace('{estado}',$texto_estado , $html);

							return $response = $mpdfService->generatePdfResponse($html);
					}else{
						$data = array('status' => 'error',
										'code' => 401,
										'data' => "No existen facturas");
					}

				
			}else{
					$data = array("status" => 'error',
									"code" => 400,
									"data" => "Los datos enviados son invalidos");
				}
			
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
		
	}

	protected function ceros($numero,$ceros=8){
		return sprintf("%0".$ceros."s", $numero );
	}

	public function cuadresAction(Request $request){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$json = $request->get('json',null);
			$params = json_decode($json);
			if ($json != null) {
				$desde = (isset($params->desde)) ? $params->desde:null;
				$hasta = (isset($params->hasta)) ? $params->hasta:null;
				if ($desde != null && $hasta != null) {
					$em = $this->getDoctrine()->getManager();
					$db = $em->getConnection();
					$query = "SELECT IF(SUM(f.total_fac) is null,0,SUM(f.total_fac)) AS ventas,(SELECT IF(SUM(d.costo_detp) is null,0,SUM(d.costo_detp)) FROM detalles_productos d WHERE d.estado_detp = 1 AND (d.created_at >= :desde AND d.created_at <= :hasta)) AS costos FROM facturas f WHERE f.estado_fac != 3 AND (f.fecha_fac >= :desde AND f.fecha_fac <= :hasta)";
					$stmt = $db->prepare($query);
	     			$params = array('desde' => $desde,'hasta' => $hasta);
	      			$stmt->execute($params);
	      			$cuadre = $stmt->fetchAll();
						$data = array('status' => 'success',
										'code' => 200,
										'data' => $cuadre
									);

				}else{
					$data = array("status" => 'error',
									"code" => 400,
									"data" => "Los datos enviados son invalidos");
				}
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Asegurese de enviar el objeto json');
			} 
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

	public function backupAction(Request $request){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			exec('c:\WINDOWS\system32\cmd.exe /c START C:\backup.bat');
			$data = array('status' => 'success',
							'code' => 200,
							'data' => "Script ejecutado verifique si el archivo se ha creado");
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

}