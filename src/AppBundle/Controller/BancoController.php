<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Services\Helpers;
use AppBundle\Services\JwtAuth;
use BackendBundle\Entity\Cuenta;
use BackendBundle\Entity\Transferencia;

class BancoController extends Controller {
	public function nuevaCuentaAction(Request $request){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$json = $request->get('json',null);
			$params = json_decode($json);
			if ($json != null) {
				$numero = (isset($params->numero)) ? $params->numero:null;
				$banco = (isset($params->banco)) ? $params->banco:null;
				$tipo = (isset($params->tipo)) ? $params->tipo:null;
				$nombre = (isset($params->nombre)) ? $params->nombre:null;
				$createdAt = new \Datetime("now");
				if ($numero != null && $banco != null && $tipo != null && $nombre != null) {
					$cuenta = new Cuenta();
					$cuenta->setNumeroCue($numero);
					$cuenta->setBancoCue($banco);
					$cuenta->setTipoCue($tipo);
					$cuenta->setNombreCue($nombre);
					$cuenta->setEstadoCue(1);
					$cuenta->setCreatedAt($createdAt);


					$em = $this->getDoctrine()->getManager();
					$cuenta_existe = $em->getRepository('BackendBundle:Cuenta')->findBy(array('numeroCue' => $numero,'estadoCue' => 1));
					if (count($cuenta_existe) == 0) {
						$em->persist($cuenta);
						$em->flush();
						$data = array("status" => 'success',
										"code" => 200,
										"data" => "Cuenta creada correctamente");
					}else{
						$data = array('status' => 'error',
										'code' => 400,
										'data' => "La cuenta ya existe");
					}
				}else{
					$data = array("status" => 'error',
									"code" => 400,
									"data" => "Los datos enviados son invalidos");
				}
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Asegurese de enviar el objeto json');
			} 
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	} 

	public function editarCuentaAction(Request $request, $id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$json = $request->get('json',null);
			$params = json_decode($json);
			if ($json != null) {
				$numero = (isset($params->numero)) ? $params->numero:null;
				$banco = (isset($params->banco)) ? $params->banco:null;
				$tipo = (isset($params->tipo)) ? $params->tipo:null;
				$nombre = (isset($params->nombre)) ? $params->nombre:null;
				if ($numero != null && $banco != null && $tipo != null && $nombre != null) {
					$em = $this->getDoctrine()->getManager();
					$cuenta = $em->getRepository('BackendBundle:Cuenta')->findOneBy(array('idCue' => $id,'estadoCue' => 1));
					if (count($cuenta) > 0) {
						$cuenta->setNumeroCue($numero);
						$cuenta->setBancoCue($banco);
						$cuenta->setTipoCue($tipo);
						$cuenta->setNombreCue($nombre);

						$em->persist($cuenta);
						$em->flush();
						$data = array('status' => 'success',
										'code' => 200,
										'data' => 'Cuenta modificada correctamente');
					}else{
						$data = array('status' => 'error',
										'code' => 400,
										'data' => 'La cuenta no existe');
					}
				}else{
					$data = array('status' => 'error',
									'code' => 400,
									'data' => 'Los datos enviados son invalidos');
				}
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Asegurese de enviar el objeto json');
			} 
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

	public function eliminarCuentaAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$cuenta = $em->getRepository('BackendBundle:Cuenta')->findOneBy(array('idCue' => $id, 'estadoCue' => 1));
				if (count($cuenta) > 0) {
					$cuenta->setEstadoCue(2);

					$em->persist($cuenta);
					$em->flush();
					$data = array('status' => 'success',
                      				'code' => 200,
                        			'data' => 'Cuenta eliminada correctamente');
				}else{
					$data = array('status' => 'error',
									'code' => 400,
									'data' => 'La cuenta no existe');	
				}
			}else{
					$data = array('status' => 'eror',
									'code' => 400,
									'data' => 'Los datos enviados son invalidos');
				}
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}

		return $helpers->json($data);
	}

	public function buscarCuentaAction(Request $request){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$em = $this->getDoctrine()->getManager();
			$dql = "SELECT c FROM BackendBundle:Cuenta c WHERE c.estadoCue = 1";//estadoCue = 1 --> activo
			$query = $em->createQuery($dql);
			$cuentas = $query->getResult();
			if (count($cuentas) > 0) {
				$data = array('status' => 'success',
								'code' => 200,
								'data' => $cuentas);
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'No existen cuentas');
			}
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}
		return $helpers->json($data);
	}

	public function buscarUnoAction(Request $request, $id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$dql = "SELECT c FROM BackendBundle:Cuenta c WHERE c.idCue = :id AND c.estadoCue = 1";
				$query = $em->createQuery($dql)->setParameter('id',$id);
				$cuenta = $query->setMaxResults(1)->getOneOrNullResult();
				if (count($cuenta) > 0) {
					$data = array('status' => 'success',
								'code' => 200,
								'data' => $cuenta);
				}else{
					$data = array('status' => 'error',
								'code' => 400,
								'data' => 'La cuenta no existe');
				}
				
			}else{
					$data = array('status' => 'error',
									'code' => 400,
									'data' => 'Los datos enviados son incorrectos');	
				}
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}
		return $helpers->json($data);
	}

	public function buscadorCuentaAction(Request $request, $search){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($search)) {
				$em = $this->getDoctrine()->getManager();
				$dql = "SELECT c FROM BackendBundle:Cuenta c WHERE c.nombreCue LIKE :search OR c.numeroCue LIKE :search AND c.estadoCue = 1";
				$query = $em->createQuery($dql)->setParameter('search',"%$search%");
				$cuentas = $query->getResult();
				if (count($cuentas) > 0) {
					$data = array('status' => 'success',
								'code' => 200,
								'data' => $cuentas);
				}else{
					$data = array('status' => 'error',
								'code' => 401,
								'data' => 'No se han encontrado cuentas');	
				}
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Los datos enviados son invalidos');
			}
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}
		return $helpers->json($data);
	}

	public function nuevaTransferenciaAction(Request $request){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$json = $request->get('json',null);
			$params = json_decode($json);
			if ($json != null) {
				$cuenta = (isset($params->cuenta)) ? $params->cuenta:null;
				$baucher = (isset($params->baucher)) ? $params->baucher:null;
				$valor = (isset($params->valor)) ? $params->valor:null;
				$tipo = (isset($params->tipo)) ? $params->tipo:null;
				$createdAt = new \Datetime('now');
				if ($cuenta != null && $baucher != null && $valor != null && $tipo != null) {
					$em = $this->getDoctrine()->getManager();
					$cuenta = $em->getRepository('BackendBundle:Cuenta')->findOneBy(array('idCue' => $cuenta));
					$transferencia = new Transferencia();
					$transferencia->setIdCue($cuenta);
					$transferencia->setNumeroTrans($baucher);
					$transferencia->setValorTrans($valor);
					$transferencia->setTipoTrans($tipo);
					$transferencia->setEstadoTrans(1);
					// $transferencia->setCreatedAt($createdAt);

					
					$transferencia_existe = $em->getRepository('BackendBundle:Transferencia')->findBy(array('numeroTrans' => $baucher,'estadoTrans' => 1));
					if (count($transferencia_existe) == 0) {
						$em->persist($transferencia);
						$em->flush();
						$data = array('status' => 'success',
								'code' => 200,
								'data' => 'Transferencia creada correctamente');

					}else{
						$data = array('status' => 'error',
										'code' => 400,
										'data' => "La transferencia ya existe");
					}

				}else{
					$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Los datos enviados son invalidos');
				}
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Asegurese de enviar el objeto json');
			} 
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

	public function historialTransferenciaAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$dql = "SELECT t.idTrans,t.numeroTrans,t.valorTrans,t.tipoTrans FROM BackendBundle:Transferencia t WHERE t.idCue = :id AND t.estadoTrans = 1";
				$query = $em->createQuery($dql)->setParameter('id',$id);
				$transferencias = $query->getResult();
				if (count($transferencias) > 0) {
					$data = array('status' => 'success',
									'code' => 200,
									'data' => $transferencias);
				}else{
					$data = array('status' => 'error',
									'code' => 400,
									'data' => 'No existen transferencias');
				}
			}else{
					$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Los datos enviados son invalidos');
				}
			
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

	public function eliminarTransferenciaAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$transferencia = $em->getRepository('BackendBundle:Transferencia')->findOneBy(array('idTrans' => $id, 'estadoTrans' => 1));
				if (count($transferencia) > 0) {
					$transferencia->setEstadoTrans(2);

					$em->persist($transferencia);
					$em->flush();
					$data = array('status' => 'success',
                      				'code' => 200,
                        			'data' => 'Transferencia eliminada correctamente');
				}else{
					$data = array('status' => 'error',
									'code' => 400,
									'data' => 'La transferencia no existe');	
				}
			}else{
					$data = array('status' => 'eror',
									'code' => 400,
									'data' => 'Los datos enviados son invalidos');
				}
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}

		return $helpers->json($data);
	}
}