<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Services\Helpers;
use AppBundle\Services\JwtAuth;
use BackendBundle\Entity\Producto;
use BackendBundle\Entity\Categoria;
use BackendBundle\Entity\CuentaPp;
use BackendBundle\Entity\DetalleProducto;

class InventarioController extends Controller{
	public function nuevaCategoriaAction(Request $request){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$json = $request->get('json',null);
			$params = json_decode($json);
			if ($json != null) {
				$nombre = (isset($params->nombre)) ? $params->nombre:null;
				$tipo = (isset($params->tipo)) ? $params->tipo:null;
				$createdAt = new \Datetime("now");
				if ($nombre != null && $tipo != null) {
					$categoria = new Categoria();
					$categoria->setNombreCat($nombre);
					$categoria->setTipoCat($tipo);
					$categoria->setEstadoCat(1);
					$categoria->setCreatedAt($createdAt);

					$em = $this->getDoctrine()->getManager();
					$categoria_existe = $em->getRepository('BackendBundle:Categoria')->findBy(array('nombreCat' => $nombre, 'estadoCat' => 1));
					if (count($categoria_existe) == 0) {
						$em->persist($categoria);
						$em->flush();
						$data = array('status' => 'success',
										'code' => 200,
										'data' => "Categoria creada correctamente");
					}else{
						$data = array('status' => 'error',
										'code' => 400,
										'data' => "La categoria ya existe");
					}
				}else{
					$data = array("status" => 'error',
									"code" => 400,
									"data" => "Los datos enviados son invalidos");
				}
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Asegurese de enviar el objeto json');
			}
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

	public function eliminarCategoriaAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$categoria = $em->getRepository('BackendBundle:Categoria')->findOneBy(array('idCat' => $id, 'estadoCat' => 1));
				if (count($categoria) > 0) {
					$categoria->setEstadoCat(2);
					$em->persist($categoria);
					$em->flush();
					$data = array('status' => 'success',
										'code' => 200,
										'data' => "Categoria eliminada correctamente");

				}else{
					$data = array('status' => 'error',
									'code' => 400,
									'data' => 'La categoria no existe');
				}
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Los datos enviados son invalidos');
			}
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

	public function buscarCategoriaAction(Request $request){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$em = $this->getDoctrine()->getManager();
			$dql = "SELECT c FROM BackendBundle:Categoria c WHERE c.estadoCat = 1";//estadoCat = 1 --> sactivo
			$query = $em->createQuery($dql);
			$categorias = $query->getResult();
			if (count($categorias) > 0) {
				$data = array('status' => 'success',
								'code' => 200,
								'data' => $categorias);
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'No existen categorias');
			}
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}
		return $helpers->json($data);
	}

	public function buscadorCategoriaAction(Request $request, $search){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if ($search != null) {
				$em = $this->getDoctrine()->getManager();
				$dql = "SELECT c FROM BackendBundle:Categoria c WHERE c.nombreCat LIKE :search AND c.estadoCat = 1";
				$query = $em->createQuery($dql)->setParameter('search',"%$search%");
				$categorias = $query->getResult();
				if (count($categorias) > 0) {
					$data = array('status' => 'success',
								'code' => 200,
								'data' => $categorias);
				}else{
					$data = array('status' => 'error',
								'code' => 401,
								'data' => 'No se han encontrado categorias');
				}
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Los datos enviados son invalidos');
			}
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}
		return $helpers->json($data);
	}

	public function buscarUnaCategoriaAction(Request $request, $id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$dql = "SELECT c FROM BackendBundle:Categoria c WHERE c.idCat = :id AND c.estadoCat = 1";
				$query = $em->createQuery($dql)->setParameter('id',$id);
				$categoria = $query->setMaxResults(1)->getOneOrNullResult();
				if (count($categoria) > 0) {
					$data = array('status' => 'success',
								'code' => 200,
								'data' => $categoria);
				}else{
					$data = array('status' => 'error',
								'code' => 400,
								'data' => 'La categoria no existe');
				}

			}else{
					$data = array('status' => 'error',
									'code' => 400,
									'data' => 'Los datos enviados son incorrectos');
				}
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}
		return $helpers->json($data);
	}

	public function nuevoProductoAction(Request $request){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$json = $request->get('json',null);
			$params = json_decode($json);
			if ($json != null) {
				$nombre = (isset($params->nombre)) ? $params->nombre:null;
				$id_categoria = (isset($params->categoria)) ? $params->categoria:null;
				$codigo = (isset($params->codigo)) ? $params->codigo:null;
				$itbis = (isset($params->itbis)) ? $params->itbis:0;
				$salida1 = (isset($params->salida1)) ? $params->salida1:null;
				$salida2 = (isset($params->salida2)) ? $params->salida2:null;
				$salida3 = (isset($params->salida3)) ? $params->salida3:null;
				$codigopa = (isset($params->codigopa)) ? $params->codigopa:null;
				$cantidadpa = (isset($params->cantidadpa)) ? $params->cantidadpa:null;
				$preciopa = (isset($params->preciopa)) ? $params->preciopa:null;
				$cantidadca = (isset($params->cantidadca)) ? $params->cantidadca:null;
				$precioca = (isset($params->precioca)) ? $params->precioca:null;
				$especial = (isset($params->especial)) ? $params->especial:null;
				$createdAt = date("d-m-Y");
				if ($nombre != null && $id_categoria != null && $codigo != null && $salida1 != null && $codigopa != null && $cantidadpa != null && $preciopa != null && $cantidadca != null && $precioca != null && $especial != null) {
					$em = $this->getDoctrine()->getManager();
					$producto = new Producto();
					$producto->setNombreProd($nombre);
					$producto->setCodigoProd($codigo);
					$producto->setItbisProd($itbis);
					$producto->setSalida1Prod($salida1);
					$producto->setSalida2Prod($salida2);
					$producto->setSalida3Prod($salida3);
					$producto->setEstadoProd(1);
					$producto->setCreatedAt($createdAt);
					$producto->setPreciopaProd($preciopa);
					$producto->setCantidadpaProd($cantidadpa);
					$producto->setCodigopaProd($codigopa);
					$producto->setCantidadcaProd($cantidadca);
					$producto->setPreciocaProd($precioca);
					$producto->setEspecialProd($especial);
					$categoria = $em->getRepository('BackendBundle:Categoria')->findOneBy(array('idCat' => $id_categoria));
					$producto->setIdCat($categoria);
					$producto_existe = $em->getRepository('BackendBundle:Producto')->findBy(array('codigoProd' => $codigo,'estadoProd' => 1));
					if (count($producto_existe) == 0) {
						$em->persist($producto);
						$em->flush();
						$data = array('status' => 'success',
									'code' => 200,
									'data' => 'Producto registrado correctamente');
					}else{
						$data = array("status" => 'error',
										"code" => 400,
										"data" => "El producto ya existe");
					}


				}else{
					$data = array("status" => 'error',
									"code" => 400,
									"data" => "Los datos enviados son invalidos");
				}
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Asegurese de enviar el objeto json');
			}
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}
	public function nuevoDetalleAction(Request $request, $id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token', null);
		if ($jwt_auth->checktoken($token)) {
			$json = $request->get('json',null);
			$params = json_decode($json);
			if ($json != null) {
				$id_proveedor = (isset($params->proveedor)) ? $params->proveedor:null;
				$fecha = (isset($params->fecha)) ? $params->fecha:null;
				$costo = (isset($params->costo)) ? $params->costo:null;
				$condicion = (isset($params->condicion)) ? $params->condicion:null;
				$cantidad = (isset($params->cantidad)) ? $params->cantidad:null;
				$createdAt = date("Y-m-d");
				if ($id_proveedor != null && $fecha != null && $costo != null && $condicion != null && $cantidad != null && is_numeric($id)) {
					$em = $this->getDoctrine()->getManager();
					$detalle = new DetalleProducto();
					$detalle->setCostoDetp($costo);
					$detalle->setCondicionDetp($condicion);
					$detalle->setCantidadDetp($cantidad);
					$detalle->setEstadoDetp(1);
					$detalle->setCreatedAt($createdAt);

					$proveedor = $em->getRepository('BackendBundle:Proveedor')->findOneBy(array('idProv' => $id_proveedor));
					$detalle->setIdProv($proveedor);

					$producto = $em->getRepository('BackendBundle:Producto')->findOneBy(array('idProd' => $id));
					$detalle->setIdProd($producto);

					$detalle->setFechaDetp($fecha);


					$em->persist($detalle);
					$em->flush();
					$ultimo = $detalle->getIdDetp();
					if ($condicion == 1) {
						$cuentapp = new CuentaPp();
						$cuentapp->setIdProv($proveedor);
						$cuentapp->setConceptoCupp('Compra del articulo : '.$producto->getNombreProd());
						$cuentapp->setTotalCupp($costo*$cantidad);
						$cuentapp->setEstadoCupp(1);
						$cuentapp->setIdDetp($ultimo);
						$em->persist($cuentapp);
						$em->flush();
					}

					$data = array('status' => 'success',
								'code' => 200,
								'data' => 'Agregado correctamente');

				}else{
					$data = array("status" => 'error',
									"code" => 400,
									"data" => "Los datos enviados son invalidos");
				}
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Asegurese de enviar el objeto json');
			}
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

	public function eliminarProductoAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$producto = $em->getRepository('BackendBundle:Producto')->findOneBy(array('idProd' => $id, 'estadoProd' => 1));
				if (count($producto) > 0) {
					$producto->setEstadoProd(2);
					$detalles = $em->getRepository('BackendBundle:DetalleProducto')->findBy(array('idProd' => $id, 'estadoDetp' => 1));
					foreach ($detalles as $detalle) {
						$detalle->setEstadoDetp(2);
						$em->persist($detalle);
					}
					$em->persist($producto);
					$em->flush();
					$data = array('status' => 'success',
								'code' => 200,
								'data' => 'Producto eliminado correctamente');
				}else{
					$data = array('status' => 'error',
									'code' => 400,
									'data' => 'El producto no existe');
				}

			}else{
				$data = array("status" => 'error',
								"code" => 400,
								"data" => "Los datos enviados son invalidos");
				}
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}
		return $helpers->json($data);
	}

	public function eliminarDetalleAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$detalle = $em->getRepository('BackendBundle:DetalleProducto')->findOneBy(array('idDetp' => $id, 'estadoDetp' => 1));
				if (count($detalle) > 0) {
					$detalle->setEstadoDetp(2);
					$cuentapp = $em->getRepository('BackendBundle:CuentaPp')->findOneBy(array('idDetp' => $id, 'estadoCupp' => 1));
					if (count($cuentapp) > 0) {
						$cuentapp->setEstadoCupp(2);
						$em->persist($cuentapp);
					}
					$em->persist($detalle);
					$em->flush();
					$data = array('status' => 'success',
									'code' => 200,
									'data' => 'Detalle eliminado correctamente');
				}else{
					$data = array('status' => 'error',
									'code' => 400,
									'data' => 'El detalle no existe');
				}
			}else{
				$data = array("status" => 'error',
								"code" => 400,
								"data" => "Los datos enviados son invalidos");
				}
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}
		return $helpers->json($data);
	}

	public function editarProductoAction(Request $request, $id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$json = $request->get('json',null);
			$params = json_decode($json);
			if ($json != null) {
				$nombre = (isset($params->nombre)) ? $params->nombre:null;
				$salida1 = (isset($params->salida1)) ? $params->salida1:null;
				$salida2 = (isset($params->salida2)) ? $params->salida2:null;
				$salida3 = (isset($params->salida3)) ? $params->salida3:null;
				$id_categoria = (isset($params->categoria)) ? $params->categoria:null;
				$codigo = (isset($params->codigo)) ? $params->codigo:null;
				$itbis = (isset($params->itbis)) ? $params->itbis:null;
				$cantidadpa = (isset($params->cantidadpa)) ? $params->cantidadpa:null;
				$preciopa = (isset($params->preciopa)) ? $params->preciopa:null;
				$cantidadca = (isset($params->cantidadca)) ? $params->cantidadca:null;
				$precioca = (isset($params->precioca)) ? $params->precioca:null;
				$codigopa = (isset($params->codigopa)) ? $params->codigopa:null;
				if ($nombre != null && $salida1 != null && $id_categoria != null && $codigo != null && is_numeric($id) && $cantidadpa != null && $preciopa != null && $cantidadca != null && $precioca != null && $codigo != null && $codigopa != null) {
					$em = $this->getDoctrine()->getManager();
					$producto = $em->getRepository('BackendBundle:Producto')->findOneBy(array('idProd' => $id, 'estadoProd' => 1));
					if (count($producto) > 0) {
						$producto->setNombreProd($nombre);
						$producto->setSalida1Prod($salida1);
						$producto->setSalida2Prod($salida2);
						$producto->setSalida3Prod($salida3);
						$producto->setCantidadpaProd($cantidadpa);
						$producto->setPreciopaProd($preciopa);
						$producto->setCantidadcaProd($cantidadca);
						$producto->setPreciocaProd($precioca);
						$producto->setCodigoProd($codigo);
						$producto->setCodigopaProd($codigopa);
						$categoria = $em->getRepository('BackendBundle:Categoria')->findOneBy(array('idCat' => $id_categoria, 'estadoCat' => 1));
						$producto->setIdCat($categoria);

						$producto->setCodigoProd($codigo);
						$producto->setItbisProd($itbis);

						$em->persist($producto);
						$em->flush();
						$data = array('status' => 'success',
										'code' => 200,
										'data' => 'Producto modificado correctamente');
					}else{
						$data = array('status' => 'error',
									'code' => 400,
									'data' => 'El producto no existe');
					}
				}else{
				$data = array("status" => 'error',
								"code" => 400,
								"data" => "Los datos enviados son invalidos");
				}
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Asegurese de enviar el objeto json');
			}
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}
	public function crearInventarioAction(Request $request){
		$data = array('status' => 'success',
								'code' => 200,
								'data' => "No se creo");
		$helpers = $this->get(Helpers::class);
		$em = $this->getDoctrine()->getManager();
		$db = $em->getConnection();
		$query = "SELECT p.id_prod FROM productos p WHERE p.estado_prod = 1";
			$stmt = $db->prepare($query);
      $params = array();
      $stmt->execute($params);
      $productos = $stmt->fetchAll();
			if (count($productos) > 0) {
			
				foreach ($productos as $producto) {
					$id_producto = $producto['id_prod'];
					$detalle = new DetalleProducto();
					$detalle->setCostoDetp(20);
					$detalle->setCondicionDetp(2);
					$detalle->setCantidadDetp(800);
					$detalle->setEstadoDetp(1);
					$detalle->setCreatedAt("2018-12-04");

					$proveedor = $em->getRepository('BackendBundle:Proveedor')->findOneBy(array('idProv' => 1));
					$detalle->setIdProv($proveedor);

					$producto2 = $em->getRepository('BackendBundle:Producto')->findOneBy(array('idProd' => $id_producto));
					$detalle->setIdProd($producto2);

					$detalle->setFechaDetp("2018-12-04");


					$em->persist($detalle);
					$em->flush();
					$data = array('status' => 'success',
								'code' => 200,
								'data' => "se creo");

				}
			}

			return $helpers->json($data);
		}

	public function buscarProductoAction(Request $request){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$em = $this->getDoctrine()->getManager();
			$db = $em->getConnection();
			$query = "SELECT p.id_prod,p.nombre_prod,p.codigo_prod,p.codigopa_prod,p.salida1_prod,p.salida2_prod,p.salida3_prod,p.itbis_prod,p.cantidadpa_prod,p.preciopa_prod,p.cantidadca_prod,p.precioca_prod,(SELECT IF(SUM(d.cantidad_detp) is null,0,SUM(d.cantidad_detp)) FROM detalles_productos d WHERE d.id_prod = p.id_prod AND d.estado_detp = 1) AS cantidad FROM productos p WHERE p.estado_prod = 1 ORDER BY p.id_prod DESC LIMIT 10";
			$stmt = $db->prepare($query);
      $params = array();
      $stmt->execute($params);
      $productos = $stmt->fetchAll();
			if (count($productos) > 0) {
				$nuevo = [];
				foreach ($productos as $producto) {
					$id_producto = $producto['id_prod'];
					$dql2 = "SELECT ABS(DATE_DIFF(CURRENT_DATE(),d.fechaDetp)) AS diferencia FROM BackendBundle:DetalleProducto d WHERE d.estadoDetp = 1 AND d.idProd = :id AND d.fechaDetp != 1";
					$query2 = $em->createQuery($dql2)->setParameter('id',$id_producto);
					$diferencias = $query2->getResult();
					$vence = 2;
					$minimo = 2;
					if ($producto['cantidad'] <= ($producto['cantidadpa_prod'] * 10) AND $producto['cantidad'] != 0) {
						$minimo = 1;
					}

					foreach ($diferencias as $diferencia) {
						if ($diferencia['diferencia'] <= 30) {
							$vence = 1;
						}
					}
					$pnuevo = array('nombre' => $producto['nombre_prod'],
									'salida1' => $producto['salida1_prod'],
									'salida2' => $producto['salida2_prod'],
									'salida3' => $producto['salida3_prod'],
									'codigo' => $producto['codigo_prod'],
									'codigopa' => $producto['codigopa_prod'],
									'itbis' => $producto['itbis_prod'],
									'idProd' => $producto['id_prod'],
									'cantidadpa' => $producto['cantidadpa_prod'],
									'preciopa' => $producto['preciopa_prod'],
									'cantidadca' => $producto['cantidadca_prod'],
									'precioca' => $producto['precioca_prod'],
									'vence' => $vence,
									'cantidad' => $producto['cantidad'],
									'minimo' => $minimo);
					array_push($nuevo,$pnuevo);
				}
				$data = array('status' => 'success',
								'code' => 200,
								'data' => $nuevo);
				}else{
					$data = array('status' => 'error',
								'code' => 401,
								'data' => 'No existen productos');
				}
			}else{
				$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

	public function buscadorProductoAction(Request $request,$search){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if ($search != null) {
				$em = $this->getDoctrine()->getManager();
				$db = $em->getConnection();
				$query = "SELECT p.id_prod,p.nombre_prod,p.codigo_prod,p.codigopa_prod,p.salida1_prod,p.salida2_prod,p.salida3_prod,p.itbis_prod,p.cantidadpa_prod,p.preciopa_prod,p.cantidadca_prod,p.precioca_prod,(SELECT IF(SUM(d.cantidad_detp) is null,0,SUM(d.cantidad_detp)) FROM detalles_productos d WHERE d.id_prod = p.id_prod AND d.estado_detp = 1) AS cantidad FROM productos p INNER JOIN categorias c ON p.id_cat = c.id_cat  WHERE p.estado_prod = 1 AND (p.nombre_prod LIKE :search OR p.codigo_prod LIKE :search OR c.nombre_cat LIKE :search OR p.codigopa_prod LIKE :search) ORDER BY p.id_prod";
				$stmt = $db->prepare($query);
	      $params = array('search' => "%$search%");
	      $stmt->execute($params);
	      $productos = $stmt->fetchAll();
				if (count($productos) > 0) {
					$nuevo = [];
					foreach ($productos as $producto) {
						$id_producto = $producto['id_prod'];
						//SELECT DATEDIFF(CURRENT_DATE,fecha_detp) AS diferencia FROM detalles_productos WHERE estado_detp = 1
						$dql2 = "SELECT ABS(DATE_DIFF(CURRENT_DATE(),d.fechaDetp)) AS diferencia FROM BackendBundle:DetalleProducto d WHERE d.estadoDetp = 1 AND d.idProd = :id AND d.fechaDetp != 1";
						$query2 = $em->createQuery($dql2)->setParameter('id',$id_producto);
						$diferencias = $query2->getResult();
						$vence = 2;
						$minimo = 2;
						if ($producto['cantidad'] <= ($producto['cantidadpa_prod'] * 10) AND $producto['cantidad'] != 0) {
							$minimo = 1;
						}

						foreach ($diferencias as $diferencia) {
							if ($diferencia['diferencia'] <= 30) {
								$vence = 1;
							}
						}
						$pnuevo = array('nombre' => $producto['nombre_prod'],
										'salida1' => $producto['salida1_prod'],
										'salida2' => $producto['salida2_prod'],
										'salida3' => $producto['salida3_prod'],
										'codigo' => $producto['codigo_prod'],
										'codigopa' => $producto['codigopa_prod'],
										'itbis' => $producto['itbis_prod'],
										'idProd' => $producto['id_prod'],
										'cantidadpa' => $producto['cantidadpa_prod'],
										'preciopa' => $producto['preciopa_prod'],
										'cantidadca' => $producto['cantidadca_prod'],
										'precioca' => $producto['precioca_prod'],
										'vence' => $vence,
										'cantidad' => $producto['cantidad'],
										'minimo' => $minimo);
						array_push($nuevo,$pnuevo);
					}
					$data = array('status' => 'success',
									'code' => 200,
									'data' => $nuevo);
					}else{
						$data = array('status' => 'error',
									'code' => 401,
									'data' => 'No existen productos');
					}

			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Los datos enviados son invalidos');
			}
		}else{
				$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

	public function buscarUnoProductoAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$dql = "SELECT p.nombreProd,p.salida1Prod,p.salida2Prod,p.salida3Prod,c.idCat,p.codigoProd,p.codigopaProd,p.itbisProd,p.cantidadpaProd,p.preciopaProd,p.cantidadcaProd,p.preciocaProd,c.nombreCat FROM BackendBundle:Producto p INNER JOIN BackendBundle:Categoria c WITH p.idCat = c.idCat WHERE p.estadoProd = 1 AND p.idProd = :id";
				$query = $em->createQuery($dql)->setParameter('id',$id);
				$producto = $query->getResult();
				if (count($producto) > 0) {
					$data = array('status' => 'success',
									'code' => 200,
									'data' => $producto);
				}else{
						$data = array('status' => 'error',
									'code' => 401,
									'data' => 'El producto no existe');
					}
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Los datos enviados son invalidos');
			}
		}else{
				$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

	public function buscarDetalleAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$dql = "SELECT d.idDetp,d.fechaDetp,d.costoDetp,d.cantidadDetp,d.condicionDetp,p.nombreProv,ABS(DATE_DIFF(CURRENT_DATE(),d.fechaDetp)) AS diferencia FROM BackendBundle:DetalleProducto d INNER JOIN BackendBundle:Proveedor p WITH d.idProv = p.idProv WHERE d.idProd = :id AND d.estadoDetp = 1";
				$query = $em->createQuery($dql)->setParameter('id',$id);
				$detalles = $query->getResult();
				if (count($detalles) > 0) {
					$data = array('status' => 'success',
									'code' => 200,
									'data' => $detalles);
				}else{
						$data = array('status' => 'error',
									'code' => 401,
									'data' => 'No existen detalles');
					}
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Los datos enviados son invalidos');
			}
		}else{
				$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

	public function buscarProductoVentaAction(Request $request,$tipo){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($tipo)) {
				$em = $this->getDoctrine()->getManager();
				$db = $em->getConnection();
				switch ($tipo) {
					case 1:
						$sql = "SELECT p.nombre_prod,p.salida1_prod as precio,p.codigo_prod,p.itbis_prod,p.id_prod,SUM(d.cantidad_detp) as cantidad FROM productos p INNER JOIN detalles_productos d ON d.id_prod = p.id_prod WHERE p.estado_prod = 1 AND d.estado_detp = 1 GROUP BY d.id_prod LIMIT 10";
						break;
					case 2:
						$sql = "SELECT p.nombre_prod,p.salida2_prod as precio,p.codigo_prod,p.itbis_prod,p.id_prod,SUM(d.cantidad_detp) as cantidad FROM productos p INNER JOIN detalles_productos d ON d.id_prod = p.id_prod WHERE p.estado_prod = 1 AND d.estado_detp = 1 GROUP BY d.id_prod LIMIT 10";
						break;
					case 3:
						$sql = "SELECT p.nombre_prod,p.salida3_prod as precio,p.codigo_prod,p.itbis_prod,p.id_prod,SUM(d.cantidad_detp) as cantidad FROM productos p INNER JOIN detalles_productos d ON d.id_prod = p.id_prod WHERE p.estado_prod = 1 AND d.estado_detp = 1 GROUP BY d.id_prod LIMIT 10";
						break;
					default:
						$data = array('status' => 'error',
									'code' => 400,
									'data' => 'El tipo enviado no es valido');
						return $helpers->json($data);
						break;
				}

				$stmt = $db->prepare($sql);
       			$params = array();
        		$stmt->execute($params);
				$productos = $stmt->fetchAll();
				if (count($productos) > 0) {
					$data = array('status' => 'success',
									'code' => 200,
									'data' => $productos);
					}else{
						$data = array('status' => 'error',
									'code' => 401,
									'data' => 'No existen productos');
					}
				}else{
					$data = array('status' => 'error',
									'code' => 400,
									'data' => 'Los datos enviados son invalidos');
				}
		}else{
				$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

	public function buscadorProductoVentaAction(Request $request,$tipo,$search){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($tipo) && $search != null) {
				$em = $this->getDoctrine()->getManager();
				$db = $em->getConnection();
				$sql = "SELECT p.nombre_prod,p.salida1_prod as precio,p.codigo_prod,p.itbis_prod,p.id_prod,p.especial_prod,IF(p.codigopa_prod = :search2,1,2) as paquete,SUM(d.cantidad_detp) as cantidad FROM productos p INNER JOIN detalles_productos d ON d.id_prod = p.id_prod WHERE p.estado_prod = 1 AND d.estado_detp = 1 AND (p.codigo_prod LIKE :search OR p.codigopa_prod LIKE :search OR p.nombre_prod LIKE :search) GROUP BY d.id_prod";

				$stmt = $db->prepare($sql);
        		$params = array(':search' => "%$search%",':search2' => $search);
        		$stmt->execute($params);
				$productos = $stmt->fetchAll();
				if (count($productos) > 0) {
					$data = array('status' => 'success',
									'code' => 200,
									'data' => $productos);
					}else{
						$data = array('status' => 'error',
									'code' => 401,
									'data' => 'No existen productos');
					}
				}else{
					$data = array('status' => 'error',
									'code' => 400,
									'data' => 'Los datos enviados son invalidos');
				}
		}else{
				$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

}
