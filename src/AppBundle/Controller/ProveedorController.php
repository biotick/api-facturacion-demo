<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Services\Helpers;
use AppBundle\Services\JwtAuth;
use BackendBundle\Entity\Proveedor;

class ProveedorController extends Controller {

	public function nuevoAction(Request $request){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$json = $request->get('json',null);
			$params = json_decode($json);
			if ($json != null) {
				$nombre = (isset($params->nombre)) ? $params->nombre:null;
				$descripcion = (isset($params->descripcion)) ? $params->descripcion:null;
				$telefono1 = (isset($params->telefono1)) ? $params->telefono1:null;
				$telefono2 = (isset($params->telefono2)) ? $params->telefono2:null;
				$telefono3 = (isset($params->telefono3)) ? $params->telefono3:null;
				$createdAt = new \Datetime("now");
				if ($nombre != null && $descripcion != null && $telefono1 != null && $telefono2 != null && $telefono3 != null) {
					$proveedor = new Proveedor();
					$proveedor->setNombreProv($nombre);
					$proveedor->setDescripcionProv($descripcion);
					$proveedor->setTelefono1Prov($telefono1);
					$proveedor->setTelefono2Prov($telefono2);
					$proveedor->setTelefono3Prov($telefono3);
					$proveedor->setEstadoProv(1);
					$proveedor->setCreatedAt($createdAt);

					$em = $this->getDoctrine()->getManager();
					$proveedor_existe = $em->getRepository('BackendBundle:Proveedor')->findBy(array('nombreProv' => $nombre,'telefono1Prov' => $telefono1, 'estadoProv' => 1));
					if (count($proveedor_existe) == 0) {
						$em->persist($proveedor);
						$em->flush();
						$data = array("status" => 'success',
										"code" => 200,
										"data" => "Proveedor creado correctamente");
					}else{
						$data = array('status' => 'error',
										'code' => 400,
										'data' => "El proveedor ya existe");
					}
				}else{
					$data = array("status" => 'error',
									"code" => 400,
									"data" => "Los datos enviados son invalidos");
				}
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Asegurese de enviar el objeto json');
			}
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}
		return $helpers->json($data);
	}

	public function editarAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$json = $request->get('json',null);
			$params = json_decode($json);
			if ($json != null) {
				$nombre = (isset($params->nombre)) ? $params->nombre:null;
				$descripcion = (isset($params->descripcion)) ? $params->descripcion:null;
				$telefono1 = (isset($params->telefono1)) ? $params->telefono1:null;
				$telefono2 = (isset($params->telefono2)) ? $params->telefono2:null;
				$telefono3 = (isset($params->telefono3)) ? $params->telefono3:null;
				if ($nombre != null && $descripcion != null && $telefono1 != null && $telefono2 != null && $telefono3 != null) {
					$em = $this->getDoctrine()->getManager();
					$proveedor = $em->getRepository('BackendBundle:Proveedor')->findOneBy(array('idProv' => $id,'estadoProv' => 1));
					if (count($proveedor) > 0) {
						$proveedor->setNombreProv($nombre);
						$proveedor->setDescripcionProv($descripcion);
						$proveedor->setTelefono1Prov($telefono1);
						$proveedor->setTelefono2Prov($telefono2);
						$proveedor->setTelefono3Prov($telefono3);

						$em->persist($proveedor);
						$em->flush();

						$data = array('status' => 'success',
										'code' => 200,
										'data' => 'Proveedor modificado correctamente');
					}else{
						$data = array('status' => 'error',
										'code' => 400,
										'data' => 'El proveedor no existe');
					}

				}else{
					$data = array('status' => 'error',
									'code' => 400,
									'data' => 'Los datos enviados son invalidos');
				}


			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Asegurese de enviar el objeto json');
			}

		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}

		return $helpers->json($data);
	}

	public function eliminarAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$proveedor = $em->getRepository('BackendBundle:Proveedor')->findOneBy(array('idProv' => $id,'estadoProv' => 1));
				if (count($proveedor) > 0) {
					$proveedor->setEstadoProv(2);
					$em->persist($proveedor);
					$em->flush();
					$data = array('status' => 'success',
                      				'code' => 200,
                        			'data' => 'Proveedor eliminado correctamente');
				}else{
					$data = array('status' => 'error',
									'code' => 400,
									'data' => 'El proveedor no existe');
				}
			}else{
					$data = array('status' => 'eror',
									'code' => 400,
									'data' => 'Los datos enviados son invalidos');
				}
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}

		return $helpers->json($data);
	}

	public function buscarAction(Request $request){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$em = $this->getDoctrine()->getManager();
			$dql = "SELECT p FROM BackendBundle:Proveedor p WHERE p.estadoProv = 1";//estado = 1 --> activo
			$query = $em->createQuery($dql);
			$proveedores = $query->getResult();
			if (count($proveedores) > 0) {
				$data = array('status' => 'success',
								'code' => 200,
								'data' => $proveedores);
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'No existen usuarios');
			}
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}
		return $helpers->json($data);
	}

	public function buscarUnoAction(Request $request, $id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$dql = "SELECT p FROM BackendBundle:Proveedor p WHERE p.idProv = :id AND p.estadoProv = 1";
				$query = $em->createQuery($dql)->setParameter('id',$id);
				$proveedor = $query->setMaxResults(1)->getOneOrNullResult();
				if (count($proveedor) > 0) {
					$data = array('status' => 'success',
								'code' => 200,
								'data' => $proveedor);
				}else{
					$data = array('status' => 'error',
								'code' => 400,
								'data' => 'El proveedor no existe');
				}

			}else{
					$data = array('status' => 'error',
									'code' => 400,
									'data' => 'Los datos enviados son incorrectos');
				}
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}
		return $helpers->json($data);
	}
		public function buscadorAction(Request $request, $search){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if ($search != null) {
				$em = $this->getDoctrine()->getManager();
				$dql = "SELECT p FROM BackendBundle:Proveedor p WHERE (p.nombreProv LIKE :search OR p.telefono1Prov LIKE :search) AND p.estadoProv = 1";
				$query = $em->createQuery($dql)->setParameter('search',"%$search%");
				$proveedores = $query->getResult();
				if (count($proveedores) > 0) {
					$data = array('status' => 'success',
								'code' => 200,
								'data' => $proveedores);
				}else{
					$data = array('status' => 'error',
								'code' => 401,
								'data' => 'No se han encontrado proveedores');
				}
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Los datos enviados son invalidos');
			}
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}
		return $helpers->json($data);
	}

	public function historialAction(Request $request, $id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token');
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$dql = "SELECT p.idProd,p.nombreProd,d.costoDetp,d.cantidadDetp,d.costoDetp*d.cantidadDetp AS total,p.createdAt FROM BackendBundle:Producto p INNER JOIN BackendBundle:DetalleProducto d WITH p.idProd = d.idProd WHERE d.idProv = :id AND p.estadoProd = 1 ORDER BY p.idProd DESC";
				$query = $em->createQuery($dql)->setParameter('id',$id);
				$historial = $query->getResult();
				if (count($historial) > 0) {
					$data = array('status' => 'success',
								'code' => 200,
								'data' => $historial);
				}else{
					$data = array('status' => 'error',
								'code' => 401,
								'data' => 'No se han encontrado productos');
				}
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Los datos enviados son invalidos');
			}

		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}
		return $helpers->json($data);
	}

	public function buscarCuentasAction(Request $request){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token');
		if ($jwt_auth->checktoken($token)) {
			$em = $this->getDoctrine()->getManager();
			$dql = "SELECT c.idCupp,c.conceptoCupp,c.totalCupp,c.estadoCupp,p.nombreProv FROM BackendBundle:CuentaPp c INNER JOIN BackendBundle:Proveedor p WITH c.idProv = p.idProv WHERE c.estadoCupp = 1";
			$query = $em->createQuery($dql);
			$query->setMaxResults(10);
			$cuentas = $query->getResult();
			if (count($cuentas) > 0) {
				$data = array('status' => 'success',
								'code' => 200,
								'data' => $cuentas);
			}else{
					$data = array('status' => 'error',
								'code' => 401,
								'data' => 'No se han encontrado cuentas');
				}
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}

		return $helpers->json($data);
	}

	public function buscadorCuentasAction(Request $request,$search){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token');
		if ($jwt_auth->checktoken($token)) {
			$em = $this->getDoctrine()->getManager();
			$dql = "SELECT c.idCupp,c.conceptoCupp,c.totalCupp,c.estadoCupp,p.nombreProv FROM BackendBundle:CuentaPp c INNER JOIN BackendBundle:Proveedor p WITH c.idProv = p.idProv WHERE c.estadoCupp = 1 AND p.nombreProv LIKE :search";
			$query = $em->createQuery($dql)->setParameter('search',"%$search%");
			$cuentas = $query->getResult();
			if (count($cuentas) > 0) {
				$data = array('status' => 'success',
								'code' => 200,
								'data' => $cuentas);
			}else{
					$data = array('status' => 'error',
								'code' => 401,
								'data' => 'No se han encontrado cuentas');
				}
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}

		return $helpers->json($data);
	}

	public function eliminarCuentaAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$cuenta = $em->getRepository('BackendBundle:CuentaPp')->findOneBy(array('idCupp' => $id,'estadoCupp' => 1));
				if (count($cuenta) > 0) {
					$cuenta->setEstadoCupp(2);
					$em->persist($cuenta);
					$em->flush();
					$data = array('status' => 'success',
                      				'code' => 200,
                        			'data' => 'Cuenta eliminado correctamente');
				}else{
					$data = array('status' => 'error',
									'code' => 400,
									'data' => 'La cuenta no existe');
				}
			}else{
					$data = array('status' => 'eror',
									'code' => 400,
									'data' => 'Los datos enviados son invalidos');
				}
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}

		return $helpers->json($data);
	}
}
