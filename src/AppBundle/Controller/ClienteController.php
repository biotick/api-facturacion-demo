<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Services\Helpers;
use AppBundle\Services\JwtAuth;
use BackendBundle\Entity\Cliente;

class ClienteController extends Controller {

	public function nuevoAction(Request $request){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$json = $request->get('json',null);
			$params = json_decode($json);

			if ($json != null) {
				$nombre = (isset($params->nombre)) ? $params->nombre:null;
				$apellido = (isset($params->apellido)) ? $params->apellido:null;
				$telefono = (isset($params->telefono)) ? $params->telefono:null;
				$direccion = (isset($params->direccion)) ? $params->direccion:null;
				$tipo = (isset($params->tipo)) ? $params->tipo:null;
				$createdAt = new \Datetime("now");
				if ($nombre != null && $apellido != null && $telefono != null && $direccion != null && $tipo != null) {
					$cliente = new Cliente();
					$cliente->setNombreCli($nombre);
					$cliente->setApellidoCli($apellido);
					$cliente->setTelefonoCli($telefono);
					$cliente->setDireccionCli($direccion);
					$cliente->setTipoCli($tipo);
					$cliente->setEstadoCli(1);
					$cliente->setCreatedAt($createdAt);

					$em = $this->getDoctrine()->getManager();
					$cliente_existe = $em->getRepository('BackendBundle:Cliente')->findBy(array('nombreCli' => $nombre,'apellidoCli' => $apellido,'estadoCli' => 1));
					if (count($cliente_existe) == 0) {
						$em->persist($cliente);
						$em->flush();
						$data = array("status" => 'success',
										"code" => 200,
										"data" => "Cliente creado correctamente");
					}else{
						$data = array('status' => 'error',
										'code' => 400,
										'data' => "El cliente ya existe");
					}
				}else{
					$data = array("status" => 'error',
									"code" => 400,
									"data" => "Los datos enviados son invalidos");
				}
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Asegurese de enviar el objeto json');
			}
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

	public function editarAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$json = $request->get('json',null);
			$params = json_decode($json);
			if ($json != null) {
				$nombre = (isset($params->nombre)) ? $params->nombre:null;
				$apellido = (isset($params->apellido)) ? $params->apellido:null;
				$direccion = (isset($params->direccion)) ? $params->direccion:null;
				$telefono = (isset($params->telefono)) ? $params->telefono:null;
				$tipo = (isset($params->tipo)) ? $params->tipo:null;
				if ($nombre != null && $apellido != null && $direccion != null && $telefono != null && $tipo != null) {
					$em = $this->getDoctrine()->getManager();
					$cliente = $em->getRepository('BackendBundle:Cliente')->findOneBy(array('idCli' => $id, 'estadoCli' => 1));
					if (count($cliente) > 0) {
						$cliente->setNombreCli($nombre);
						$cliente->setApellidoCli($apellido);
						$cliente->setDireccionCli($direccion);
						$cliente->setTelefonoCli($telefono);
						$cliente->setTipoCli($tipo);

						$em->persist($cliente);
						$em->flush();
						$data = array('status' => 'success',
										'code' => 200,
										'data' => "Cliente modificado correctamente");

					}else{
						$data = array('status' => 'error',
										'code' => 400,
										'data' => "El cliente no existe");
					}
				}else{
					$data = array("status" => 'error',
									"code" => 400,
									"data" => "Los datos enviados son invalidos");
				}
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Asegurese de enviar el objeto json');
			}

		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

	public function eliminarAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$cliente = $em->getRepository('BackendBundle:Cliente')->findOneBy(array('idCli' => $id, 'estadoCli' => 1));
				if (count($cliente) > 0) {
					$cliente->setEstadoCli(2);
					$em->persist($cliente);
					$em->flush();
					$data = array('status' => 'success',
										'code' => 200,
										'data' => "Cliente eliminado correctamente");

				}else{
						$data = array('status' => 'error',
										'code' => 400,
										'data' => "El cliente no existe");
					}

			}else{
					$data = array("status" => 'error',
									"code" => 400,
									"data" => "Los datos enviados son invalidos");
				}
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

	public function buscarAction(Request $request){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$em = $this->getDoctrine()->getManager();
			$dql = "SELECT c FROM BackendBundle:Cliente c WHERE c.estadoCli = 1";//estadoCli = 1 --> cliente activo
			$query = $em->createQuery($dql);
			$clientes = $query->getResult();
			$nuevos = [];
			foreach ($clientes as $cliente) {
				$id = $cliente->getIdCli();

				#SELECT SUM(total_fac) AS total FROM facturas a INNER JOIN clientes b ON b.id_cli = a.id_cli WHERE b.id_cli=1 AND a.created_at BETWEEN ADDDATE(CURDATE(),INTERVAL -30 DAY) AND CURDATE();

				$dql2 = "SELECT SUM(f.totalFac) AS total FROM BackendBundle:Factura f INNER JOIN BackendBundle:Cliente c WITH c.idCli = f.idCli WHERE c.idCli = :id AND f.createdAt BETWEEN :dias AND CURRENT_DATE()";
				$query2 = $em->createQuery($dql2)->setParameters(array('id' => $id,'dias' => -30));
				$suma = $query2->setMaxResults(1)->getOneOrNullResult();
				$nuevo = array('nombreCli' => $cliente->getNombreCli(),
								'idCli' => $cliente->getIdCli(),
								'apellidoCli' => $cliente->getApellidoCli(),
								'direccionCli' => $cliente->getDireccionCli(),
								'telefonoCli' => $cliente->getTelefonoCli(),
								'tipoCli' => $cliente->getTipoCli(),
								'meta' => 2);
				if($suma['total'] >= 50000 && $suma['total'] != null){
					$nuevo['meta'] = 1;
				}

				array_push($nuevos, $nuevo);
			}
			if (count($nuevos) > 0) {
				$data = array('status' => 'success',
							'code' => 200,
							'data' => $nuevos);
			}else{
					$data = array('status' => 'error',
								'code' => 401,
								'data' => 'No se han encontrado clientes');
				}

		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

	public function buscarUnoAction(Request $request, $id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$dql = "SELECT c FROM BackendBundle:Cliente c WHERE c.idCli = :id AND c.estadoCli = 1";
				$query = $em->createQuery($dql)->setParameter('id',$id);
				$cliente = $query->setMaxResults(1)->getOneOrNullResult();
				if (count($cliente) > 0) {
					$data = array('status' => 'success',
								'code' => 200,
								'data' => $cliente);
				}else{
					$data = array('status' => 'error',
								'code' => 400,
								'data' => 'El cliente no existe');
				}

			}else{
					$data = array('status' => 'error',
									'code' => 400,
									'data' => 'Los datos enviados son incorrectos');
				}
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}
		return $helpers->json($data);
	}

	public function buscadorAction(Request $request, $search){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if ($search != null) {
				$em = $this->getDoctrine()->getManager();
				$dql = "SELECT c FROM BackendBundle:Cliente c WHERE ( c.nombreCli LIKE :search OR c.apellidoCli LIKE :search) AND c.estadoCli = 1";
				$query = $em->createQuery($dql)->setParameter('search',"%$search%");
				$clientes = $query->getResult();
				$nuevos = [];
				foreach ($clientes as $cliente) {
					$id = $cliente->getIdCli();

					#SELECT SUM(total_fac) AS total FROM facturas a INNER JOIN clientes b ON b.id_cli = a.id_cli WHERE b.id_cli=1 AND a.created_at BETWEEN ADDDATE(CURDATE(),INTERVAL -30 DAY) AND CURDATE();

					$dql2 = "SELECT SUM(f.totalFac) AS total FROM BackendBundle:Factura f INNER JOIN BackendBundle:Cliente c WITH c.idCli = f.idCli WHERE c.idCli = :id AND f.createdAt BETWEEN :dias AND CURRENT_DATE()";
					$query2 = $em->createQuery($dql2)->setParameters(array('id' => $id,'dias' => -30));
					$suma = $query2->setMaxResults(1)->getOneOrNullResult();
					$nuevo = array('nombreCli' => $cliente->getNombreCli(),
									'idCli' => $cliente->getIdCli(),
									'apellidoCli' => $cliente->getApellidoCli(),
									'direccionCli' => $cliente->getDireccionCli(),
									'telefonoCli' => $cliente->getTelefonoCli(),
									'tipoCli' => $cliente->getTipoCli(),
									'meta' => 2);
					if($suma['total'] >= 50000 && $suma['total'] != null){
						$nuevo['meta'] = 1;
					}

					array_push($nuevos, $nuevo);
				}
				if (count($nuevos) > 0) {
					$data = array('status' => 'success',
								'code' => 200,
								'data' => $nuevos);
				}else{
					$data = array('status' => 'error',
								'code' => 401,
								'data' => 'No se han encontrado clientes');
				}
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Los datos enviados son invalidos');
			}
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}
		return $helpers->json($data);
	}
}
