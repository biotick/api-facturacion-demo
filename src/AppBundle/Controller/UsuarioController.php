<?php 
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Services\Helpers;
use AppBundle\Services\JwtAuth;
use BackendBundle\Entity\Usuario;

class UsuarioController extends Controller {
	
	public function nuevoAction(Request $request){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$json = $request->get('json',null);
			$params = json_decode($json);
			if ($json !=null) {
				$nombre = (isset($params->nombre)) ? $params->nombre:null;
				$apellido = (isset($params->apellido)) ? $params->apellido:null;
				$tipo = (isset($params->tipo)) ? $params->tipo:null;
				$nick = (isset($params->nick)) ? $params->nick:null;
				$clave = (isset($params->clave)) ? $params->clave:null;
				$createdAt = new \Datetime("now");
				if ($nombre != null && $apellido != null && $tipo != null && $nick != null && $clave != null) {
					$usuario = new Usuario();
					$usuario->setNombreUsu($nombre);
					$usuario->setApellidoUsu($apellido);
					$usuario->setTipoUsu($tipo);
					$usuario->setNickUsu($nick);
					$usuario->setEstadoUsu(1);
					$pwd = hash('sha256',$clave);
					$usuario->setClaveUsu($pwd);
					$usuario->setCreatedAt($createdAt);

					$em = $this->getDoctrine()->getManager();
					$usuario_existe = $em->getRepository("BackendBundle:Usuario")->findBy(array("nickUsu" => $nick,"estadoUsu" => 1));
					if (count($usuario_existe) == 0) {
						$em->persist($usuario);
						$em->flush();
						$data = array("status" => 'success',
										"code" => 200,
										"data" => "Usuario creado correctamente");
					}else{
						$data = array('status' => 'error',
										'code' => 400,
										'data' => "El usuario ya existe");
					}
				}else{
					$data = array("status" => 'error',
									"code" => 400,
									"data" => "Los datos enviados son invalidos");
				}

			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Asegurese de enviar el objeto json');
			} 
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);

	}

	public function editarAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$json = $request->get('json',null);
			$params = json_decode($json);
			if ($json != null) {
				$nombre = (isset($params->nombre)) ? $params->nombre:null;
				$apellido = (isset($params->apellido)) ? $params->apellido:null;
				$tipo = (isset($params->tipo)) ? $params->tipo:null;
				$nick = (isset($params->nick)) ? $params->nick:null;
				$clave = (isset($params->clave)) ? $params->clave:null;
				if ($nombre != null && $apellido != null && $tipo != null && $nick != null && is_numeric($id)) {
					$em = $this->getDoctrine()->getManager();
					$usuario = $em->getRepository('BackendBundle:Usuario')->findOneBy(array('idUsu' => $id, 'estadoUsu' => 1));
					if (count($usuario) > 0) {
						$usuario->setNombreUsu($nombre);
						$usuario->setApellidoUsu($apellido);
						$usuario->setTipoUsu($tipo);
						$usuario->setNickUsu($nick);

						if ($clave != null) {
							$pwd = hash('sha256',$clave);
							$usuario->setClaveUsu($pwd);
						}

						$em->persist($usuario);
						$em->flush();

						$data = array('status' => 'success',
										'code' => 200,
										'data' => 'Usuario modificado correctamente');
					}else{
						$data = array('status' => 'error',
										'code' => 400,
										'data' => 'El usuario no existe');
					}

				}else{
					$data = array('status' => 'error',
									'code' => 400,
									'data' => 'Los datos enviados son invalidos');
				}

				
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Asegurese de enviar el objeto json');
			} 
				
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}

		return $helpers->json($data);
	}

	public function eliminarAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$usuario = $em->getRepository('BackendBundle:Usuario')->findOneBy(array('idUsu' => $id, 'estadoUsu' => 1));
				if (count($usuario) > 0) {
					$usuario->setEstadoUsu(2);

					$em->persist($usuario);
					$em->flush();
					$data = array('status' => 'success',
                      				'code' => 200,
                        			'data' => 'Usuario eliminado correctamente');
				}else{
					$data = array('status' => 'error',
									'code' => 400,
									'data' => 'El usuario no existe');	
				}
			}else{
					$data = array('status' => 'eror',
									'code' => 400,
									'data' => 'Los datos enviados son invalidos');
				}
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}

		return $helpers->json($data);
	}

	public function buscarAction(Request $request){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$em = $this->getDoctrine()->getManager();
			$dql = "SELECT u.idUsu,u.nombreUsu,u.apellidoUsu,u.tipoUsu,u.nickUsu FROM BackendBundle:Usuario u WHERE u.estadoUsu = 1";//estadoUsu = 1 --> usuario activo
			$query = $em->createQuery($dql);
			$usuarios = $query->getResult();
			if (count($usuarios) > 0) {
				$data = array('status' => 'success',
								'code' => 200,
								'data' => $usuarios);
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'No existen usuarios');
			}
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}
		return $helpers->json($data);
	}

	public function buscarUnoAction(Request $request, $id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$dql = "SELECT u.idUsu,u.nombreUsu,u.apellidoUsu,u.tipoUsu,u.nickUsu FROM BackendBundle:Usuario u WHERE u.idUsu = :id AND u.estadoUsu = 1";
				$query = $em->createQuery($dql)->setParameter('id',$id);
				$usuario = $query->setMaxResults(1)->getOneOrNullResult();
				if (count($usuario) > 0) {
					$data = array('status' => 'success',
								'code' => 200,
								'data' => $usuario);
				}else{
					$data = array('status' => 'error',
								'code' => 400,
								'data' => 'El usuario no existe');
				}
				
			}else{
					$data = array('status' => 'error',
									'code' => 400,
									'data' => 'Los datos enviados son incorrectos');	
				}
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}
		return $helpers->json($data);
	}

	public function buscadorAction(Request $request, $search){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if ($search != null) {
				$em = $this->getDoctrine()->getManager();
				$dql = "SELECT u.idUsu,u.nombreUsu,u.apellidoUsu,u.tipoUsu,u.nickUsu FROM BackendBundle:Usuario u WHERE u.nombreUsu LIKE :search OR u.apellidoUsu LIKE :search AND u.estadoUsu = 1";
				$query = $em->createQuery($dql)->setParameter('search',"%$search%");
				$usuarios = $query->getResult();
				if (count($usuarios) > 0) {
					$data = array('status' => 'success',
								'code' => 200,
								'data' => $usuarios);
				}else{
					$data = array('status' => 'error',
								'code' => 401,
								'data' => 'No se han encontrado usuarios');	
				}
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Los datos enviados son invalidos');
			}
		}else{
			$data = array('status' => 'error',
							'code' => 500,
							'data' => 'Token invalido');
		}
		return $helpers->json($data);
	}
}	