<?php
namespace AppBundle\Controller;

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Services\Helpers;
use AppBundle\Services\JwtAuth;
use BackendBundle\Entity\Temporal;
use BackendBundle\Entity\Factura;
use BackendBundle\Entity\DetalleFactura;


class FacturaController extends Controller{
	public function nuevoTemporalAction(Request $request){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$json = $request->get('json');
			$params = json_decode($json);
			if ($json != null) {
				$id_producto = (isset($params->producto)) ? $params->producto:null;
				$cantidad = (isset($params->cantidad)) ? $params->cantidad:null;
				$precio = (isset($params->precio)) ? $params->precio:null;
				$paquete = (isset($params->paquete)) ? $params->paquete:null;
				$id_usu = (isset($params->usuario)) ? $params->usuario:null;
				if ($id_producto != null && $cantidad != null && $precio != null && $id_usu != null) {
					$em = $this->getDoctrine()->getManager();
					$producto = $em->getRepository('BackendBundle:Producto')->findOneBy(array('idProd' => $id_producto));
					$usuario = $em->getRepository('BackendBundle:Usuario')->findOneBy(array('idUsu' => $id_usu));
					if (count($producto) > 0) {
						$cantidadpa = $producto->getCantidadpaProd();
						$preciopa = $producto->getPreciopaProd();
						$cantidadca = $producto->getCantidadcaProd();
						$precioca = $producto->getPreciocaProd();
						if ($paquete == 1) {
							$cantidad = $cantidad * $cantidadpa;
						}
						if ($cantidad >= $cantidadpa) {
							$precio = $preciopa;
						}
						if ($cantidad >= $cantidadca) {
							$precio = $precioca;
						}
						$temporal = new Temporal();
						$temporal->setIdPro($producto);
						$temporal->setCantidadTem($cantidad);
						$temporal->setPrecioTem($precio);
						$temporal->setIdUsu($usuario);
						$producto_existe = $em->getRepository('BackendBundle:Temporal')->findOneBy(array('idPro' => $id_producto,'idUsu' => $id_usu));
						if (count($producto_existe) == 0) {
							$em->persist($temporal);
							$em->flush();
							$data = array("status" => 'success',
											"code" => 200,
											"data" => "Producto agregado correctamente");
						}else{
							$cantidad_v = $producto_existe->getCantidadTem() + $cantidad;
							$producto_existe->setCantidadTem($cantidad_v);
							$em->persist($producto_existe);
							$em->flush();
							$data = array("status" => 'success',
											"code" => 200,
											"data" => "Producto agregado correctamente");

						}
					}else{
						$data = array('status' => 'error',
										'code' => 400,
										'data' => 'No existe el producto');
					}
					
				}else{
					$data = array("status" => 'error',
									"code" => 400,
									"data" => "Los datos enviados son invalidos");
				}	
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Asegurese de enviar el objeto json');
			} 
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

	public function buscarTemporalAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$em = $this->getDoctrine()->getManager();
			$dql = "SELECT t.idTem,t.cantidadTem,t.precioTem,p.nombreProd,p.codigoProd,p.itbisProd FROM BackendBundle:Temporal t INNER JOIN BackendBundle:Producto p WITH t.idPro = p.idProd INNER JOIN BackendBundle:Usuario u WITH t.idUsu = u.idUsu WHERE p.estadoProd = 1 AND u.idUsu = :id";
			$query = $em->createQuery($dql)->setParameter('id',$id);
			$temporales = $query->getResult();
			if (count($temporales) > 0) {
				$data = array('status' => 'success',
								'code' => 200,
								'data' => $temporales);
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'No existen productos');
			}
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

	public function eliminarTemporalAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$temporal = $em->getRepository('BackendBundle:Temporal')->findOneBy(array('idTem' => $id));
				if (count($temporal) > 0) {
					$em->remove($temporal);
					$em->flush();
					$data = array('status' => 'success',
									'code' => 200,
									'data' => 'Producto eliminado correctamente');
				}else{
					$data = array('status' => 'error',
								  'code' => 400,
								  'data' => 'No existe el producto');
			}
			}else{
				$data = array("status" => 'error',
								"code" => 400,
								"data" => "Los datos enviados son invalidos");
			}
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}
	public function imprimir4aAction(Request $request){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$json = $request->get('json',null);
			$params = json_decode($json);
			if ($json != null) {
				$codigo = $this->ceros($this->codigoFactura());
				$id_cli = (isset($params->cliente)) ? $params->cliente:null;
				$estado = (isset($params->estado)) ? $params->estado:1;
				$id_usu = (isset($params->usuario)) ? $params->usuario:null;
				$fecha = date("Y-m-d");
				$condicion = (isset($params->condicion)) ? $params->condicion:1;
				$descuento = (isset($params->descuento)) ? $params->descuento:0;
				$subtotal = (isset($params->subtotal)) ? $params->subtotal:null;
				$total = (isset($params->total)) ? $params->total:null;
				$tipo = (isset($params->tipo)) ? $params->tipo:null;
				$ncf = $this->obtenerNCF($tipo);
				$rnc = (isset($params->rnc)) ? $params->rnc:null;
				$imprimir = (isset($params->imprimir)) ? $params->imprimir:1;
				$createdAt = new \Datetime('now');
				if ($codigo != null && $id_cli != null && $id_usu != null && $condicion != null && $subtotal != null && $total != null && $tipo != null) {
					if ($ncf == '020814ByM') {
						$data = array('status' => 'error',
										'code' => 25,
										'data' => 'Se necesitan nuevos comprobantes del tipo: '.$tipo);
					}else{
						$em = $this->getDoctrine()->getManager();

						$cliente = $em->getRepository('BackendBundle:Cliente')->findOneBy(array('idCli' => $id_cli));
						$usuario = $em->getRepository('BackendBundle:Usuario')->findOneBy(array('idUsu' => $id_usu));

						$factura = new Factura();
						$factura->setCodigoFac($codigo);
						$factura->setIdCli($cliente);
						$factura->setIdUsu($usuario);
						$factura->setFechaFac($fecha);
						$factura->setCondicionFac($condicion);
						$factura->setDescuentoFac($descuento);
						$factura->setSubtotalFac($subtotal);
						$factura->setTotalFac($total);
						$factura->setNfcFac($ncf);
						$factura->setCreatedAt($createdAt);
						$factura->setEstadoFac($estado);
						$factura->setTipoFac($tipo);
						if ($rnc != null) {
							$factura->setRncFac($rnc);
						}else{
							$factura->setRncFac(1);
						}

						$factura_existe = $em->getRepository('BackendBundle:Factura')->findBy(array('codigoFac' => $codigo));
						if (count($factura_existe) == 0) {
							$em->persist($factura);
							$em->flush();
							$temporales = $em->getRepository('BackendBundle:Temporal')->findBy(array('idUsu' => $id_usu));
							foreach ($temporales as $temporal) {
								$detalle_factura = new DetalleFactura();
								$producto = $em->getRepository('BackendBundle:Producto')->findOneBy(array('idProd'=>$temporal->getIdPro()));

								$detalle_factura->setIdProd($producto);
								$detalle_factura->setCantidadDet($temporal->getCantidadTem());
								$detalle_factura->setMontoDet($temporal->getPrecioTem());
								$detalle_factura->setIdFac($factura);
								$detalle_factura->setCreatedAt($createdAt);
								$em->persist($detalle_factura);
								$em->flush();
							}
							$dql = "DELETE FROM BackendBundle:Temporal t WHERE t.idUsu = :id";
							$query = $em->createQuery($dql)->setParameter('id',$id_usu);
							$resultado = $query->getResult();
							//factura
							
							$detalles = $em->getRepository('BackendBundle:DetalleFactura')->findBy(array('idFac' => $factura->getIdFac()));
							$total_itbis = 0;
							$total = 0;

							foreach ($detalles as $detalle) {
								$producto = $em->getRepository('BackendBundle:Producto')->findOneBy(array('idProd' => $detalle->getIdProd()));
								$this->resta($detalle->getCantidadDet(),$producto->getIdProd());
								$total += $detalle->getCantidadDet() * $detalle->getMontoDet();
								$total_itbis += ($detalle->getMontoDet()*$detalle->getCantidadDet()*$producto->getItbisProd())/100;
								
							}
							
								$data = array('status' => 'success',
											'code' => 200,
											'data' => $factura->getIdFac());
							//fin factura
							
						}else{
							$data = array('status' => 'error',
											'code' => 400,
											'data' => 'La factura ya existe '.$codigo);
						}
					}

					
				}else{
					$data = array("status" => 'error',
									"code" => 400,
									"data" => "Los datos enviados son invalidos");
				}
  			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Asegurese de enviar el objeto json');
			} 
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);

	}
	public function ver4aAction(Request $request,$token,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		if ($jwt_auth->checktoken($token)) {
			$em = $this->getDoctrine()->getManager();
			$dql = "SELECT c.telefonoCli,c.nombreCli,c.apellidoCli,u.nombreUsu,u.apellidoUsu,f.fechaFac,f.codigoFac,f.totalFac,f.descuentoFac FROM BackendBundle:Factura f INNER JOIN BackendBundle:Cliente c WITH f.idCli = c.idCli INNER JOIN BackendBundle:Usuario u WITH f.idUsu = u.idUsu WHERE f.estadoFac != 3 AND f.idFac = :id";
			$query = $em->createQuery($dql)->setParameter('id',$id);
			$facturas = $query->getResult();
			if (count($facturas > 0)) {
					$dql2 = "SELECT d FROM BackendBundle:DetalleFactura d WHERE d.idFac = :id";
					$query2 = $em->createQuery($dql2)->setParameter('id',$id);
					$detalles = $query2->getResult();
					$tableta = '<tr class="info">';
					$tableta = $tableta.'<th>Cantidad</th>';
					$tableta = $tableta.'<th>Producto</th>';
					$tableta = $tableta.'<th>Precio</th>';
					// $tableta = $tableta.'<th>ITBIS</th>';
					$tableta = $tableta.'<th>Total</th>';
					$tableta = $tableta.'</tr>';
					foreach ($detalles as $detalle) {
						$producto = $em->getRepository('BackendBundle:Producto')->findOneBy(array('idProd' => $detalle->getIdProd()));
						$tableta = $tableta.'<tr>';
						$tableta = $tableta.'<td>'.$detalle->getCantidadDet().'</td>';
						$tableta = $tableta.'<td>'.$producto->getNombreProd().'</td>';
						$tableta = $tableta.'<td>$'.$detalle->getMontoDet().'</td>';
						$tableta = $tableta. '<td>$'.$detalle->getCantidadDet() * $detalle->getMontoDet().'</td>';
						$tableta = $tableta.'</tr>';
					}
					$descuento_fac = ($facturas[0]['totalFac'] * $facturas[0]['descuentoFac'])/100; 
					$subtotal = ($facturas[0]['totalFac'] + $descuento_fac);
					$tableta = $tableta.'</table>';
					$tableta = $tableta.'<br>';
					$tableta = $tableta.'<table align="right" class="cont">';
					$tableta = $tableta.'<tr>';
					$tableta = $tableta.'<td class="marg">SUBTOTAL $</th>';
					$tableta = $tableta.'<td class="marg">$'.number_format($subtotal,2,'.',',').'</td>';
					$tableta = $tableta.'</tr>';
					$tableta = $tableta.'<tr>';
          			$tableta = $tableta.'<td>DESCUENTO </th>';
          			$tableta = $tableta.'<td>$'.$descuento_fac.'</th>';
          			$tableta = $tableta.'</tr>';
					$tableta = $tableta.'<tr>';
					$tableta = $tableta.'<th class="marg">TOTAL $</th>';
					$tableta = $tableta.'<th class="marg">$'.number_format($facturas[0]['totalFac'],2,'.',',').'</th>';
					$tableta = $tableta.'</tr>';
					$tableta = $tableta.'</table>';
					$config = $em->getRepository('BackendBundle:Configuracion')->findOneBy(array('idConf'=>1));
					$mpdfService = $this->get('tfox.mpdfport');
					$css = file_get_contents('html/estilosPDF.css');
					$html = file_get_contents('html/pdf_factura.html');
					$html = str_replace('{empresa}',$config->getNombreConf(), $html);
					// $html = str_replace('{correo}','distribuidorajimenez01@gmail.com', $html);
					$html = str_replace('{telefono}',$config->getTelefonoConf(), $html);
					$html = str_replace('{direccion}',$config->getDireccionConf(), $html);
					$html = str_replace('{cliente}',$facturas[0]['nombreCli'].' '.$facturas[0]['apellidoCli'], $html);
					$html = str_replace('{vendedor}',$facturas[0]['nombreUsu'].' ' .$facturas[0]['apellidoUsu'], $html);
					$html = str_replace('{telcliente}',$facturas[0]['telefonoCli'], $html);
					
					$html = str_replace('{fecha}',$facturas[0]['fechaFac'], $html);
				
					$html = str_replace('{cod}',$facturas[0]['codigoFac'], $html);
					$html = str_replace('{tabla}',$tableta, $html);
			
					$html = str_replace('<style></style>','<style>'.$css.'</style>', $html);
					return $response = $mpdfService->generatePdfResponse($html);

			}else{
				$data = array('status' => 'error',
								'code' => 401,
								'data' => "No existen facturas");
					}
			}else{

				$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}
	public function imprimirFacturaAction(Request $request){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$json = $request->get('json',null);
			$params = json_decode($json);
			if ($json != null) {
				$codigo = $this->ceros($this->codigoFactura());
				$id_cli = (isset($params->cliente)) ? $params->cliente:null;
				$estado = (isset($params->estado)) ? $params->estado:1;
				$id_usu = (isset($params->usuario)) ? $params->usuario:null;
				$fecha = date("Y-m-d");
				$condicion = (isset($params->condicion)) ? $params->condicion:1;
				$descuento = (isset($params->descuento)) ? $params->descuento:0;
				$subtotal = (isset($params->subtotal)) ? $params->subtotal:null;
				$total = (isset($params->total)) ? $params->total:null;
				$tipo = (isset($params->tipo)) ? $params->tipo:null;
				$ncf = $this->obtenerNCF($tipo);
				$rnc = (isset($params->rnc)) ? $params->rnc:null;
				$imprimir = (isset($params->imprimir)) ? $params->imprimir:1;
				$createdAt = new \Datetime('now');
				if ($codigo != null && $id_cli != null && $id_usu != null && $condicion != null && $subtotal != null && $total != null && $tipo != null) {
					if ($ncf == '020814ByM') {
						$data = array('status' => 'error',
										'code' => 25,
										'data' => 'Se necesitan nuevos comprobantes del tipo: '.$tipo);
					}else{
						$em = $this->getDoctrine()->getManager();

						$cliente = $em->getRepository('BackendBundle:Cliente')->findOneBy(array('idCli' => $id_cli));
						$usuario = $em->getRepository('BackendBundle:Usuario')->findOneBy(array('idUsu' => $id_usu));

						$factura = new Factura();
						$factura->setCodigoFac($codigo);
						$factura->setIdCli($cliente);
						$factura->setIdUsu($usuario);
						$factura->setFechaFac($fecha);
						$factura->setCondicionFac($condicion);
						$factura->setDescuentoFac($descuento);
						$factura->setSubtotalFac($subtotal);
						$factura->setTotalFac($total);
						$factura->setNfcFac($ncf);
						$factura->setCreatedAt($createdAt);
						$factura->setEstadoFac($estado);
						$factura->setTipoFac($tipo);
						if ($rnc != null) {
							$factura->setRncFac($rnc);
						}else{
							$factura->setRncFac(1);
						}

						$factura_existe = $em->getRepository('BackendBundle:Factura')->findBy(array('codigoFac' => $codigo));
						if (count($factura_existe) == 0) {
							$em->persist($factura);
							$em->flush();
							$temporales = $em->getRepository('BackendBundle:Temporal')->findBy(array('idUsu' => $id_usu));
							foreach ($temporales as $temporal) {
								$detalle_factura = new DetalleFactura();
								$producto = $em->getRepository('BackendBundle:Producto')->findOneBy(array('idProd'=>$temporal->getIdPro()));

								$detalle_factura->setIdProd($producto);
								$detalle_factura->setCantidadDet($temporal->getCantidadTem());
								$detalle_factura->setMontoDet($temporal->getPrecioTem());
								$detalle_factura->setIdFac($factura);
								$detalle_factura->setCreatedAt($createdAt);
								$em->persist($detalle_factura);
								$em->flush();
							}
							$dql = "DELETE FROM BackendBundle:Temporal t WHERE t.idUsu = :id";
							$query = $em->createQuery($dql)->setParameter('id',$id_usu);
							$resultado = $query->getResult();
							//factura
							$tipo_factura = "FACTURA PARA CONSUMIDOR FINAL";
							if ($tipo == 2) {
								$tipo_factura = "FACTURA CON VALOR FISCAL";
							}
							$config = $em->getRepository('BackendBundle:Configuracion')->findOneBy(array('idConf' => 1));
							$nombre_impresora = "EPSON TM-T88II"; 
 							$connector = new WindowsPrintConnector($nombre_impresora);
							$printer = new Printer($connector);
							$printer->setJustification(Printer::JUSTIFY_CENTER);
							$printer->setTextSize(1, 2);
							$printer -> setEmphasis(true);
							$printer->text($config->getNombreConf() . "\n");
							$printer -> setEmphasis(false);
							$printer->setTextSize(1, 1);
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							$printer->text($config->getDireccionConf(). "\n");
							$printer->text("Tel:" .$config->getTelefonoConf()."\n");
							// $printer->text("RNC: 130090068\n");
							$printer->setJustification(Printer::JUSTIFY_CENTER);
							$printer->text("COMPROBANTE AUTORIZADO POR LA DGII\n");
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							$printer->text(date("d-m-Y") . "\n");
							$printer->text("NCF:".$ncf."\n");
							if ($tipo == 2) {
								$printer->text("RNC CLIENTE: ".$rnc."\n");
							}
							$printer->text("-----------------------------------------\n");
							$printer->setJustification(Printer::JUSTIFY_CENTER);
							$printer -> setEmphasis(true);
							$printer->text($tipo_factura."\n");
							$printer -> setEmphasis(false);
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							$printer->text("-----------------------------------------\n");
						
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							$printer->text("Factura : ".$codigo."\n");
							$printer->text("Vendedor: ".$usuario->getNombreUsu()." ".$usuario->getApellidoUsu()."\n");
							$printer->text("Cliente : ".$cliente->getNombreCli()." ".$cliente->getApellidoCli()."\n");
							$printer->text("-----------------------------------------\n");
							$printer -> setEmphasis(true);
							$printer->text("DESCRIPCION          ITBIS          VALOR\n");
							$printer -> setEmphasis(false);
							$printer->text("-----------------------------------------\n");
							$detalles = $em->getRepository('BackendBundle:DetalleFactura')->findBy(array('idFac' => $factura->getIdFac()));
							$total_itbis = 0;
							$total = 0;

							foreach ($detalles as $detalle) {
								$producto = $em->getRepository('BackendBundle:Producto')->findOneBy(array('idProd' => $detalle->getIdProd()));
								$this->resta($detalle->getCantidadDet(),$producto->getIdProd());
								$total += $detalle->getCantidadDet() * $detalle->getMontoDet();
								$total_itbis += ($detalle->getMontoDet()*$detalle->getCantidadDet()*$producto->getItbisProd())/100;
								$printer->setJustification(Printer::JUSTIFY_RIGHT);
							
								$line = sprintf('%-40.50s %26.2f %16.2f', $detalle->getCantidadDet().'x'.$detalle->getMontoDet().'-'.$producto->getNombreProd(), ($detalle->getMontoDet()*$detalle->getCantidadDet()*$producto->getItbisProd())/100, $detalle->getMontoDet() * $detalle->getCantidadDet());
								$printer->text($line);
								$printer->text("\n");
							}

							$printer->text("-----------------------------------------\n");
							$printer->setJustification(Printer::JUSTIFY_RIGHT);
							$printer->text("ITBIS: $". $total_itbis . "\n");
							$printer->text("TOTAL: $". ($total - (($total * $descuento)/100)) ."\n");
							$printer->text($config->getPieConf()."\ncodislaw.com");
							$printer->feed();
							if ($imprimir == 1) {
								$printer->cut();
								$printer->pulse();
								$printer->close();
								$data = array('status' => 'success',
											'code' => 200,
											'data' => 'La factura se ha imprimido correctamente ');
							}else{
								$data = array('status' => 'success',
											'code' => 200,
											'data' => 'La factura se ha creado correctamente ');
							}
							//fin factura
							
						}else{
							$data = array('status' => 'error',
											'code' => 400,
											'data' => 'La factura ya existe '.$codigo);
						}
					}

					
				}else{
					$data = array("status" => 'error',
									"code" => 400,
									"data" => "Los datos enviados son invalidos");
				}
  			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'Asegurese de enviar el objeto json');
			} 
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}

		return $helpers->json($data);
	}

	protected function ceros($numero,$ceros=8){
		return sprintf("%0".$ceros."s", $numero );
	}

	protected function obtenerNCF($tipo){
		if (is_numeric($tipo)) {
			$em = $this->getDoctrine()->getManager();
			switch ($tipo) {
				case 1:
					$ncf = $em->getRepository('BackendBundle:Comprobante')->findOneBy(array('idCom' => 1));
					$ultimo = $ncf->getUltimoCom();
					$desde = $ncf->getDesdeCom();
					$hasta = $ncf->getHastaCom();
					$prefijo = $ncf->getNfcCom();
					if ($ultimo == 0) {
						$ultimo = $this->ceros($desde);
						$completo = $prefijo.$ultimo;
						$ncf->setUltimoCom($ultimo);
						$em->persist($ncf);
						$em->flush();
						return $completo;
					}else{
						if ($ultimo < $hasta) {
							$ultimo = $this->ceros($ultimo + 1);
							$completo = $prefijo.$ultimo;
							$updatedAt = new \Datetime("now");
							$ncf->setUltimoCom($ultimo);
							$ncf->setUpdatedAt($updatedAt);
							$em->persist($ncf);
							$em->flush();
							return $completo;
						}else{
							return "020814ByM";
						}
					}
					break;
				case 2:
					$ncf = $em->getRepository('BackendBundle:Comprobante')->findOneBy(array('idCom' => 2));
					$ultimo = $ncf->getUltimoCom();
					$desde = $ncf->getDesdeCom();
					$hasta = $ncf->getHastaCom();
					$prefijo = $ncf->getNfcCom();
					if ($ultimo == 0) {
						$ultimo = $this->ceros($desde);
						$completo = $prefijo.$ultimo;
						$updatedAt = new \Datetime("now");
						$ncf->setUltimoCom($ultimo);
						$em->persist($ncf);
						$em->flush();
						return $completo;
					}else{
						if ($ultimo < $hasta) {
							$ultimo = $this->ceros($ultimo + 1);
							$completo = $prefijo.$ultimo;
							$updatedAt = new \Datetime("now");
							$ncf->setUltimoCom($ultimo);
							$ncf->setUpdatedAt($updatedAt);
							$em->persist($ncf);
							$em->flush();
							return $completo;
						}else{
							return "020814ByM";
						}
					}
					break;
				default:
					# code...
					break;
			}
		}else{
			return "No es un tipo valido";
		}
	}

	protected function codigoFactura(){
		$em = $this->getDoctrine()->getManager();
		$dql = "SELECT f.idFac FROM BackendBundle:Factura f ORDER BY f.idFac DESC";
		$query = $em->createQuery($dql);
		$ultimo = $query->getResult();
		if (count($ultimo) > 0) {
			return $ultimo[0]['idFac'] + 1;
		}else{
			return 1;
		}
		
	}

	public function buscarFacturaAction(Request $request){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$em = $this->getDoctrine()->getManager();
			$dql = "SELECT f.idFac,f.codigoFac,f.fechaFac,f.estadoFac,f.totalFac,f.nfcFac,c.nombreCli,u.nombreUsu FROM BackendBundle:Factura f INNER JOIN BackendBundle:Cliente c WITH c.idCli = f.idCli INNER JOIN BackendBundle:Usuario u WITH u.idUsu = f.idUsu WHERE f.estadoFac != 3 ORDER BY f.idFac DESC"; 
			$query = $em->createQuery($dql);
			$query->setMaxResults(10);
			$facturas = $query->getResult();
			if (count($facturas) > 0) {
				$data = array('status' => 'success',
								'code' => 200,
								'data' => $facturas);
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'No existen productos');
			}
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}
		return $helpers->json($data);
	}

	public function eliminarFacturaAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$factura = $em->getRepository('BackendBundle:Factura')->findOneBy(array('idFac' => $id));
				if (count($factura) > 0) {
					$factura->setEstadoFac(3);
					$em->persist($factura);
					$em->flush();
					$data = array('status' => 'success',
									'code' => 200,
									'data' => 'Factura eliminada correctamente');
				}else{
					$data = array("status" => 'error',
									"code" => 400,
									"data" => "No existe la factura");
				}
			}else{
				$data = array("status" => 'error',
								"code" => 400,
								"data" => "Los datos enviados son invalidos");
			}
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}
		return $helpers->json($data);
	}
	public function buscadorFacturaAction(Request $request,$search){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$em = $this->getDoctrine()->getManager();
			$dql = "SELECT f.idFac,f.codigoFac,f.fechaFac,f.estadoFac,f.totalFac,f.nfcFac,c.nombreCli,u.nombreUsu FROM BackendBundle:Factura f INNER JOIN BackendBundle:Cliente c WITH c.idCli = f.idCli INNER JOIN BackendBundle:Usuario u WITH u.idUsu = f.idUsu WHERE f.estadoFac != 3 AND (f.codigoFac LIKE :search OR c.nombreCli LIKE :search)"; 
			$query = $em->createQuery($dql)->setParameter('search',"%$search%");
			$facturas = $query->getResult();
			if (count($facturas) > 0) {
				$data = array('status' => 'success',
								'code' => 200,
								'data' => $facturas);
			}else{
				$data = array('status' => 'error',
								'code' => 400,
								'data' => 'No existen productos');
			}
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}
		return $helpers->json($data);
	}

	public function editarFacturaAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$json = $request->get('json',null);
			$params = json_decode($json);
			if (is_numeric($id) && $json != null) {
				$em = $this->getDoctrine()->getManager();
				$factura = $em->getRepository('BackendBundle:Factura')->findOneBy(array('idFac' => $id));
				if (count($factura) > 0) {
					$factura->setEstadoFac($params->estado);
					$em->persist($factura);
					$em->flush();
					$data = array('status' => 'success',
								'code' => 200,
								'data' => 'Factura modificada correctamente');
				}else{
					$data = array('status' => 'error',
								  'code' => 400,
								  'data' => 'No existe la factura');
				}	
			}else{
				$data = array("status" => 'error',
								"code" => 400,
								"data" => "Los datos enviados son invalidos");
			}
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}
		return $helpers->json($data);
	}

	public function buscarUnoFacturaAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$dql = "SELECT f.idFac,f.estadoFac FROM BackendBundle:Factura f WHERE f.estadoFac != 3 AND f.idFac = :id";
				$query = $em->createQuery($dql)->setParameter('id',$id);
				$factura = $query->getResult();
				if (count($factura) > 0) {
					$data = array('status' => 'success',
								   'code' => 200,
								   'data' => $factura);
				}else{
					$data = array('status' => 'error',
								  'code' => 400,
								  'data' => 'No existe la factura');
				}		
			}else{
				$data = array("status" => 'error',
								"code" => 400,
								"data" => "Los datos enviados son invalidos");
			}
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}
		return $helpers->json($data);
	}

	public function imprimirUnaFacturaAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			if (is_numeric($id)) {
				$em = $this->getDoctrine()->getManager();
				$factura = $em->getRepository('BackendBundle:Factura')->findOneBy(array('idFac' => $id));
				if (count($factura) > 0) {
					//factura
					$tipo = $factura->getTipoFac();
					$ncf = $factura->getNfcFac();
					$rnc = $factura->getRncFac();
					$codigo = $this->ceros($factura->getCodigoFac());
					$usuario = $em->getRepository('BackendBundle:Usuario')->findOneBy(array('idUsu' => $factura->getIdUsu()));
					$cliente = $em->getRepository('BackendBundle:Cliente')->findOneBy(array('idCli' => $factura->getIdCli()));
							$tipo_factura = "FACTURA PARA CONSUMIDOR FINAL";
							if ($tipo == 2) {
								$tipo_factura = "FACTURA CON VALOR FISCAL";
							}
							$nombre_impresora = "EPSON TM-T88II"; 
 							$connector = new WindowsPrintConnector($nombre_impresora);
 							$config = $em->getRepository('BackendBundle:Configuracion')->findOneBy(array('idConf' => 1));
							$printer = new Printer($connector);
							$printer->setJustification(Printer::JUSTIFY_CENTER);
							$printer->setTextSize(1, 2);
							$printer -> setEmphasis(true);
							$printer->text($config->getNombreConf() . "\n");
							$printer -> setEmphasis(false);
							$printer->setTextSize(1, 1);
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							$printer->text($config->getDireccionConf() . "\n");
							$printer->text("Tel:" .$config->getTelefonoConf()."\n");
							// $printer->text("RNC: 130090068\n");
							$printer->setJustification(Printer::JUSTIFY_CENTER);
							$printer->text("COMPROBANTE AUTORIZADO POR LA DGII\n");
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							$printer->text(date("d-m-Y") . "\n");
							$printer->text("NCF:".$ncf."\n");
							if ($tipo == 2) {
								$printer->text("RNC CLIENTE: ".$rnc."\n");
							}
							$printer->text("-----------------------------------------\n");
							$printer->setJustification(Printer::JUSTIFY_CENTER);
							$printer -> setEmphasis(true);
							$printer->text($tipo_factura."\n");
							$printer -> setEmphasis(false);
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							$printer->text("-----------------------------------------\n");
						
							$printer->setJustification(Printer::JUSTIFY_LEFT);
							$printer->text("Factura : ".$codigo."\n");
							$printer->text("Vendedor: ".$usuario->getNombreUsu()." ".$usuario->getApellidoUsu()."\n");
							$printer->text("Cliente : ".$cliente->getNombreCli()." ".$cliente->getApellidoCli()."\n");
							$printer->text("-----------------------------------------\n");
							$printer -> setEmphasis(true);
							$printer->text("DESCRIPCION          ITBIS          VALOR\n");
							$printer -> setEmphasis(false);
							$printer->text("-----------------------------------------\n");
							$detalles = $em->getRepository('BackendBundle:DetalleFactura')->findBy(array('idFac' => $factura->getIdFac()));
							$total_itbis = 0;
							$total = 0;
							foreach ($detalles as $detalle) {
								$producto = $em->getRepository('BackendBundle:Producto')->findOneBy(array('idProd' => $detalle->getIdProd()));
								$total += $detalle->getCantidadDet() * $detalle->getMontoDet();
								$total_itbis += ($detalle->getMontoDet()*$detalle->getCantidadDet()*$producto->getItbisProd())/100;
								$printer->setJustification(Printer::JUSTIFY_RIGHT);
							
								$line = sprintf('%-40.50s %26.2f %16.2f', $detalle->getCantidadDet().'x'.$detalle->getMontoDet().'-'.$producto->getNombreProd(), ($detalle->getMontoDet()*$detalle->getCantidadDet()*$producto->getItbisProd())/100, $detalle->getMontoDet() * $detalle->getCantidadDet());
								$printer->text($line);
								$printer->text("\n");
							}
							
							$printer->text("-----------------------------------------\n");
							$printer->setJustification(Printer::JUSTIFY_RIGHT);
							$printer->text("ITBIS: $". $total_itbis . "\n");
							$printer->text("TOTAL: $". $total  ."\n");
							$printer->text($config->getPieConf()."\ncodislaw.com");
							$printer->feed();
							$printer->cut();
							$printer->pulse();
							$printer->close();
							//fin factura
							$data = array('status' => 'success',
											'code' => 200,
											'data' => 'La factura se ha imprimido correctamente');
				}else{
					$data = array('status' => 'error',
								  'code' => 400,
								  'data' => 'No existe la factura');
				}	
			}else{
				$data = array("status" => 'error',
								"code" => 400,
								"data" => "Los datos enviados son invalidos");
			}
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}
		return $helpers->json($data);
	}

	protected function resta($cantidad,$idDetalle){
		$em = $this->getDoctrine()->getManager();
		$dql = "SELECT d.idDetp,d.cantidadDetp FROM BackendBundle:DetalleProducto d WHERE d.idProd = :id ORDER BY d.fechaDetp ASC";
		$query = $em->createQuery($dql)->setParameter('id',$idDetalle);
		$detalles = $query->getResult();

		if ($cantidad <= 0) {
			return;
		}

		foreach ($detalles as $detalle) {
			$id = $detalle['idDetp'];
			if ($detalle['cantidadDetp'] <= $cantidad) {
				$dql2 = "UPDATE BackendBundle:DetalleProducto d SET d.cantidadDetp = 0, d.estadoDetp = 2 WHERE d.idDetp = :id";
				$query2 = $em->createQuery($dql2)->setParameter('id',$id);
				$resultado = $query2->getResult();
				$cantidad -= $detalle['cantidadDetp'];
			}else{
				$filaCantidad = $detalle['cantidadDetp'] - $cantidad;
				$dql3 = "UPDATE BackendBundle:DetalleProducto d SET d.cantidadDetp = :cantidad WHERE d.idDetp = :id";
				$query3 = $em->createQuery($dql3)->setParameters(array('cantidad' => $filaCantidad, 'id' => $id));
				$resultado = $query3->getResult();
				$cantidad = 0;
			}

			if( $cantidad == 0 ){
        		//te sales del ciclo que recorre los productos
        		break;
       		}
		}
	}

	public function buscarComprobanteAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$em = $this->getDoctrine()->getManager();
			$comprobante = $em->getRepository('BackendBundle:Comprobante')->findOneBy(array('idCom' => $id));
			if (count($comprobante) > 0) {
				$data = array('status' => 'success',
											'code' => 200,
											'data' => $comprobante);
			}else{
				$data = array('status' => 'error',
											'code' => 400,
											'data' => 'No existe el comprobante con el id:'.$id);
			}
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}
		return $helpers->json($data);
	}

	public function editarComprobanteAction(Request $request,$id){
		$helpers = $this->get(Helpers::class);
		$jwt_auth = $this->get(JwtAuth::class);
		$token = $request->get('token',null);
		if ($jwt_auth->checktoken($token)) {
			$json = $request->get('json',null);
			$params = json_decode($json);
			if ($json != null) {
				$em = $this->getDoctrine()->getManager();
				$comprobante = $em->getRepository('BackendBundle:Comprobante')->findOneBy(array('idCom' => $id));
				if (count($comprobante) > 0) {
					$updatedAt = new \Datetime('now');
					$comprobante->setDesdeCom($params->desde);
					$comprobante->setHastaCom($params->hasta);
					$comprobante->setUltimoCom(0);
					$comprobante->setUpdatedAt($updatedAt);
					$em->persist($comprobante);
					$em->flush();
					$data = array('status' => 'success',
								'code' => 200,
								'data' => 'Comprobante modificado correctamente');

				}else{
					$data = array('status' => 'error',
											'code' => 400,
											'data' => 'No existe el comprobante con el id:'.$id);
			}
			}else{
				$data = array("status" => 'error',
								"code" => 400,
								"data" => "Los datos enviados son invalidos");
			}
		}else{
			$data = array("status" => "error",
							"code" => 500,
							"data" => "Token invalido");
		}
		return $helpers->json($data);
	}
}