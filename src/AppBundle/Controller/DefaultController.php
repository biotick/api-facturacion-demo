<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Services\Helpers;
use AppBundle\Services\JwtAuth;

class DefaultController extends Controller
{

    public function loginAction(Request $request){
      $helpers = $this->get(Helpers::class);

      //recibir datos por POST
      $json = $request->get('json',null);

      //array a devolver por defecto
      $data = array("status" => "error",
                      'data' => 'Enviar json por POST');

      if ($json != null) {// si el objeto no es invalido hacer el login

        //convertir json a un objeto php
        $params = json_decode($json);
        //llenar las credenciales
        $nick = (isset($params->nick)) ? $params->nick:null;
        $password = (isset($params->password)) ? $params->password:null;
        $getInfo = (isset($params->getInfo)) ? $params->getInfo:null;

        //cifrar contrase;a
        $pwd = hash('sha256',$password);

        if ($nick !=null && $password !=null) {
          //Si se valida el nick y la password
          $jwt_auth = $this->get(JwtAuth::class);
          //valido si se quiere la informacion del usuario
          if ($getInfo == null || $getInfo == false) {
            $singup = $jwt_auth->singup($nick,$pwd);
          }else{
            $singup = $jwt_auth->singup($nick,$pwd,true);
          }
          //retorno con el metodo nativo json el token o los datos del usuario
          return $this->json($singup);
        }else{
          //error al validar el nick o la password
          $data = array("status" => "error",
                          'data' => 'Nick o Clave incorrecto');
        }

      }

      return $helpers->json($data);
    }
}
